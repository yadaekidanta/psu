<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OfferAccepted extends Notification
{
    use Queueable;
    private $tender;
    private $offer;
    public function __construct($tender,$offer)
    {
        $this->tender = $tender;
        $this->offer = $offer;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        $tender = $this->tender;
        $offer = $this->offer;
        return (new MailMessage)
            ->subject('Penawaran diterima')
            ->view('email.offer_accept',compact('notifiable','tender','offer'));
    }
    public function toArray($notifiable)
    {
        return [
            'id' => $this->offer->id,
            'tipe' => 3,
            'judul' => $this->tender->judul,
            'nilai_penawaran' => $this->offer->nilai,
            'status' => $this->offer->st
        ];
    }
}
