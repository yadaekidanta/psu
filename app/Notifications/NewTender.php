<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewTender extends Notification
{
    use Queueable;
    private $tender;
    public function __construct($tender)
    {
        $this->tender = $tender;
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        $tender = $this->tender;
        return (new MailMessage)
            ->subject('Tender Terbaru')
            ->view('email.tender',compact('notifiable','tender'));
    }
    public function toArray($notifiable)
    {
        return [
            'id' => $this->tender->id,
            'tipe' => 1,
            'judul' => $this->tender->judul,
            'nilai' => $this->tender->nilai_tender,
            'tanggal_akhir_daftar' => $this->tender->tender_deadline_at->format('j F Y')
        ];
    }
}
