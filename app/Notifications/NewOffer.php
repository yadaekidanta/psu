<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewOffer extends Notification
{
    use Queueable;
    protected $tender;
    private $offer;
    public function __construct($tender, $offer)
    {
        $this->tender = $tender;
        $this->offer = $offer;
    }
    public function via($notifiable)
    {
        return ['database'];
    }
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }
    public function toArray($notifiable)
    {
        return [
            'id' => $this->offer->id,
            'tipe' => 2,
            'partner' => $this->offer->partner->nama_perusahaan,
            'judul' => $this->tender->judul,
            'nilai_penawaran' => $this->offer->nilai,
        ];
    }
}
