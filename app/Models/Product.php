<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "product";
    public function getImageAttribute()
    {
        if($this->gambar){
            return asset('storage/' . $this->gambar);
        }else{
            return asset('keenthemes/media/avatars/blank.png');
        }
    }
}
