<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kbli extends Model
{
    use HasFactory;
    protected $table = "kbli";
    public $timestamps = false;
    public function jenis()
    {
        return $this->belongsTo(JenisKbli::class,'jenis_kbli_id','id');
    }
}
