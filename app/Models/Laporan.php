<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    use HasFactory;
    protected $table = "laporan";
    public function jenis()
    {
        return $this->belongsTo(LaporanType::class,'laporan_type_id','id');
    }
}
