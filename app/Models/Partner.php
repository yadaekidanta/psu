<?php

namespace App\Models;

use App\Notifications\NewTender;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;

class Partner extends Authenticatable implements MustVerifyEmail
{
    use HasFactory,Notifiable;
    protected $table = "partner";
    protected $dates = ["verified_at"];
    public function getImageAttribute()
    {
        if($this->avatar){
            return asset('storage/' . $this->avatar);
        }else{
            return asset('keenthemes/media/avatars/blank.png');
        }
    }
    public static function calculate_profile($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->toArray()), PREG_GREP_INVERT);
        $per_column = 100 / count($columns);
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
    public function dokumen(){
        return $this->hasMany(PartnerDoc::class,'partner_id','id');
    }
    public function penawaran(){
        return $this->hasMany(TenderOffer::class,'partner_id','id')->get();
    }
    public function penawaran_diterima(){
        return $this->hasMany(TenderOffer::class,'partner_id','id')->where('st','=','Penawaran diterima')->get();
    }
    public function bank()
    {
        return $this->belongsTo(Bank::class,'bank_id','id');
    }
    public function type()
    {
        return $this->belongsTo(PartnerType::class,'partner_type_id','id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class,'province_id','id');
    }
    public function city()
    {
        return $this->belongsTo(City::class,'city_id','id');
    }
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class,'subdistrict_id','id');
    }
}
