<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenderOffer extends Model
{
    use HasFactory;
    protected $table = "tender_offer";
    public function tender()
    {
        return $this->belongsTo(Tender::class,'tender_id','id');
    }
    public function partner()
    {
        return $this->belongsTo(Partner::class,'partner_id','id');
    }
}
