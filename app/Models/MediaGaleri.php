<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaGaleri extends Model
{
    use HasFactory;
    protected $table = "media_galeri";
    public $timestamps = false;
    public function getImageAttribute()
    {
        if($this->gambar){
            return asset('storage/' . $this->gambar);
        }else{
            return asset('keenthemes/media/avatars/blank.png');
        }
    }
}
