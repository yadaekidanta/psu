<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class Employee extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $table = "employee";
    protected $dates = ['tanggal_lahir','tanggal_pengangkatan'];
    public function getImageAttribute()
    {
        if($this->avatar){
            return asset('storage/' . $this->avatar);
        }else{
            return asset('keenthemes/media/avatars/blank.png');
        }
    }
    public function lokasi()
    {
        return $this->belongsTo(Location::class,'lokasi_id','id');
    }
    public function divisi()
    {
        return $this->belongsTo(Divisi::class,'divisi_id','id');
    }
    public function role()
    {
        return $this->belongsTo(Role::class,'role_id','id');
    }
}