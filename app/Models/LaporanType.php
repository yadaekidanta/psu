<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaporanType extends Model
{
    use HasFactory;
    protected $table = "laporan_type";
    public $timestamps = false;
}
