<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartnerDoc extends Model
{
    use HasFactory;
    protected $table = "partner_doc";
    public function jenis()
    {
        return $this->belongsTo(DocType::class,'doc_type_id','id');
    }
}
