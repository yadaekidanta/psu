<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Tender extends Model
{
    use HasFactory, Notifiable;
    protected $table = "tender";
    protected $dates = ['tender_deadline_at','work_deadline_at','start_at','end_at','winner_at','contract_at'];
    public function list_penawaran()
    {
        return $this->hasMany(TenderOffer::class,'tender_id','id')->orderBy('created_at','DESC');
    }
}
