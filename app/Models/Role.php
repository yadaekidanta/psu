<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = "role";
    public $timestamps = false;

    public function lokasi()
    {
        return $this->belongsTo(Location::class,'lokasi_id','id');
    }
    public function divisi()
    {
        return $this->belongsTo(Divisi::class,'divisi_id','id');
    }
}
