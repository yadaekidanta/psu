<?php

namespace App\Http\Controllers\Eproc;

use App\Models\PartnerDoc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PartnerDocController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jenis_dokumen' => 'required',
            'nomor' => 'required',
            'file' => 'required|mimes:pdf',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jenis_dokumen')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_dokumen'),
                ]);
            }elseif ($errors->has('nomor')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nomor'),
                ]);
            }elseif ($errors->has('file')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file'),
                ]);
            }
        }
        $check = PartnerDoc::where('partner_id','=',Auth::guard('procurement')->user()->id)->where('doc_type_id','=',$request->jenis_dokumen)->first();
        if($check){
            return response()->json([
                'alert' => 'error',
                'message' => 'Anda sudah upload dokumen ini',
            ]);
        }
        $data = new PartnerDoc;
        $data->partner_id = Auth::guard('procurement')->user()->id;
        $data->doc_type_id = $request->jenis_dokumen;
        $data->nomor = $request->nomor;
        if(request()->file('file')){
            $file = request()->file('file')->store("partner_doc");
            $data->file = $file;
        };
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Dokumen tersimpan',
        ]);
    }
    public function download(PartnerDoc $partnerDoc)
    {
        return Storage::download($partnerDoc->file);
    }
    public function destroy(PartnerDoc $partnerDoc)
    {
        Storage::delete($partnerDoc->file);
        $partnerDoc->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Dokumen terhapus',
        ]);
    }
}
