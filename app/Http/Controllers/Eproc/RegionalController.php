<?php

namespace App\Http\Controllers\Eproc;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subdistrict;

class RegionalController extends Controller
{
    public function list_city(Request $request){
        $result = City::where('province_id','=',$request->province)->get();
        $list = "<option value=''>Pilih Kota</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
    public function list_subdistrict(Request $request){
        $result = Subdistrict::where('city_id','=',$request->city)->get();
        $list = "<option value=''>Pilih Kecamatan</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
}
