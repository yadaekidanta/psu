<?php

namespace App\Http\Controllers\Eproc;

use App\Models\Bank;
use App\Models\Kbli;
use App\Models\Partner;
use App\Models\Province;
use App\Models\PartnerDoc;
use App\Models\PartnerType;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\PasswordChangedMail;
use App\Http\Controllers\Controller;
use App\Models\DocType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index(){
        return view('page.eproc.profile.main');
    }
    public function index_profile(){
        $remove1 = Str::remove('[',Auth::guard('procurement')->user()->bidang_usaha);
        $remove2 = Str::remove('"',$remove1);
        $remove3 = Str::remove(']',$remove2);
        $mykbli = explode(',',$remove3);
        $jenis_rekanan = PartnerType::get();
        $kbli = Kbli::orderBy('kode','ASC')->get();
        return view('page.eproc.profile.edit', compact('kbli','mykbli','jenis_rekanan'));
    }
    public function index_domisili(){
        $province = Province::get();
        return view('page.eproc.profile.edit_domisili',compact('province'));
    }
    public function index_bank(){
        $bank = Bank::orderBy('nama','ASC')->get();
        return view('page.eproc.profile.edit_bank',compact('bank'));
    }
    public function index_document(Request $request){
        $jenis = DocType::get();
        if ($request->ajax()) {
            $dokumen = PartnerDoc::where('partner_id', '=', Auth::guard('procurement')->user()->id)->paginate(10);
            return view('page.eproc.profile.list_doc',compact('dokumen'));
        }
        return view('page.eproc.profile.edit_document',compact('jenis'));
    }
    public function index_password(){
        return view('page.eproc.profile.edit_password');
    }
    public function change_profile(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'hp' => 'required',
            'bidang_usaha' => 'required',
            'jenis_rekanan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('hp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('hp'),
                ]);
            }elseif ($errors->has('bidang_usaha')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bidang_usaha'),
                ]);
            }elseif ($errors->has('jenis_rekanan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_rekanan'),
                ]);
            }
        }
        $partner = Partner::where('id','=',Auth::guard('procurement')->user()->id)->first();
        $partner->email = $request->email;
        $partner->hp = $request->hp;
        $partner->bidang_usaha = $request->bidang_usaha;
        $partner->partner_type_id = $request->jenis_rekanan;
        if(request()->file('avatar')){
            Storage::delete($partner->avatar);
            $avatar = request()->file('avatar')->store("avatar");
            $partner->avatar = $avatar;
        };
        $partner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profil tersimpan',
            'redirect' => 'reload',
        ]);
    }
    public function change_domisili(Request $request){
        $validator = Validator::make($request->all(), [
            'province' => 'required',
            'city' => 'required',
            'subdistrict' => 'required',
            'alamat' => 'required',
            'kode_pos' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('province')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('province'),
                ]);
            }elseif ($errors->has('city')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('city'),
                ]);
            }elseif ($errors->has('subdistrict')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subdistrict'),
                ]);
            }elseif ($errors->has('alamat')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif ($errors->has('kode_pos')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode_pos'),
                ]);
            }
        }
        $partner = Partner::where('id','=',Auth::guard('procurement')->user()->id)->first();
        $partner->province_id = $request->province;
        $partner->city_id = $request->city;
        $partner->subdistrict_id = $request->subdistrict;
        $partner->alamat = $request->alamat;
        $partner->postcode = $request->kode_pos;
        $partner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Domisili tersimpan',
            'redirect' => 'reload',
        ]);
    }
    public function change_bank(Request $request){
        $validator = Validator::make($request->all(), [
            'bank' => 'required',
            'no_rekening' => 'required',
            'atas_nama_rekening' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('bank')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('bank'),
                ]);
            }elseif ($errors->has('no_rekening')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_rekening'),
                ]);
            }elseif ($errors->has('atas_nama_rekening')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('atas_nama_rekening'),
                ]);
            }
        }
        $partner = Partner::where('id','=',Auth::guard('procurement')->user()->id)->first();
        $partner->bank_id = $request->bank;
        $partner->no_rek = $request->no_rekening;
        $partner->rek_an = $request->atas_nama_rekening;
        $partner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Bank tersimpan',
            'redirect' => 'reload',
        ]);
    }
    public function change_document(Request $request){
        $validator = Validator::make($request->all(), [
            'nama_pendiri' => 'required',
            'no_ktp' => 'required',
            'no_npwp' => 'required',
            // 'compro' => 'mimes:pdf',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_pendiri')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_pendiri'),
                ]);
            }elseif ($errors->has('no_ktp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_ktp'),
                ]);
            }elseif ($errors->has('no_npwp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_npwp'),
                ]);
            }
        }
        $partner = Partner::where('id','=',Auth::guard('procurement')->user()->id)->first();
        $partner->nama_pendiri = $request->nama_pendiri;
        $partner->no_ktp = $request->no_ktp;
        $partner->no_npwp = $request->no_npwp;
        $partner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Dokumen tersimpan',
            'redirect' => 'reload',
        ]);
    }
    public function change_password(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            } elseif ($errors->has('password_confirmation')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password_confirmation'),
                ]);
            }
        }
        Partner::where('id', Auth::guard('procurement')->user()->id)->update(['password' => Hash::make($request->password)]);
        $users = Partner::where('id', Auth::guard('procurement')->user()->id)->first();
        Mail::to($users->email)->send(new PasswordChangedMail($users));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kata sandi tersimpan',
            'redirect' => 'reload',
        ]);
    }
    public function download_ktp(){
        return Storage::download(Auth::guard('procurement')->user()->ktp);
    }
    public function download_npwp(){
        return Storage::download(Auth::guard('procurement')->user()->npwp);
    }
    public function download_compro(){
        return Storage::download(Auth::guard('procurement')->user()->compro);
    }
}