<?php

namespace App\Http\Controllers\Eproc;

use App\Models\Partner;
use App\Models\UserVerify;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\WelcomeMailPartner;
use App\Mail\PasswordChangedMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\ConfirmationResetPasswordMail;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:procurement')->except('do_logout');
    }
    public function index()
    {
        return view('page.eproc.auth.main');
    }
    public function check_npwp(Request $request){
        $npwp = Str::remove('-','',Str::remove('.','',$request->npwp));
        $urlNpwp    = 'https://sse3.pajak.go.id/cekWsDataWp';

        $data       = array("npwp" => $npwp);
        $dataJson   = json_encode($data);

        $ch         = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlNpwp);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
            // 'Cookie: TSPD_101='.$request->token
        ));
        $result         = curl_exec($ch);
        $objectResult   = json_decode($result);
        dd($objectResult);

        if($objectResult->status == 1)
            return response()->json($objectResult, 200);
        else
            return response()->json($objectResult, 400);
    }
    public function do_login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:partner|email|max:255',
            'password' => 'required|min:8',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        if(Auth::guard('procurement')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return response()->json([
                'alert' => 'success',
                'message' => 'Selamat datang '. Auth::guard('procurement')->user()->nama,
                'callback' => 'reload',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.',
            ]);
        }
    }
    public function do_register(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'nama_perusahaan' => 'required',
            'hp' => 'required|unique:partner|min:9|max:13',
            'email' => 'required|email|unique:partner',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('nama_perusahaan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_perusahaan'),
                ]);
            }elseif ($errors->has('hp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('hp'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        $partner = new Partner;
        $partner->nama = Str::title($request->nama);
        $partner->hp = $request->hp;
        $partner->nama_perusahaan = $request->nama_perusahaan;
        $partner->email = $request->email;
        $partner->password = Hash::make($request->password);
        $partner->save();
        $token = Str::random(64);

        UserVerify::create([
            'user_id' => $partner->id,
            'token' => $token
        ]);
        Mail::to($request->email)->send(new WelcomeMailPartner($partner, $token));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kami telah mengirim email aktifasi Anda!',
            'callback' => 'page_login',
        ]);
    }
    public function do_forgot(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:partner',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $employee = Partner::where('email',$request->email)->first();

        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        $data = array(
            'token' => $token,
            'employee' => $employee
        );
        Mail::to($request->email)->send(new ConfirmationResetPasswordMail($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kami telah mengirim email tautan pemulihan kata sandi Anda!',
            'callback' => 'page_login',
        ]);
    }
    public function reset($token){
        $updatePassword = DB::table('password_resets')->where(['token' => $token])->first();
        if (!$updatePassword) {
            return redirect()->route('auth.index');
        }else{
            return view('page.office.auth.reset', compact('token'));
        }
    }
    public function do_reset(Request $request){
        $validator = Validator::make($request->all(), [
            'token' => 'required|exists:password_resets',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('token')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('token'),
                ]);
            } elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            } elseif ($errors->has('password_confirmation')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password_confirmation'),
                ]);
            }
        }
        $updatePassword = DB::table('password_resets')->where(['token' => $request->token])->first();
        if (!$updatePassword) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Invalid token!',
            ]);
        }
        Partner::where('email', $updatePassword->email)->update(['password' => Hash::make($request->password)]);
        $users = Partner::where('email', $updatePassword->email)->first();
        DB::table('password_resets')->where(['email' => $updatePassword->email])->delete();
        Mail::to($users->email)->send(new PasswordChangedMail($users));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kata sandi Anda telah diubah!',
            'callback' => 'reload',
        ]);
    }
    public function do_verify($token)
    {
        $verifyUser = UserVerify::where('token', $token)->first();

        $message = 'Maaf email Anda tidak dapat diidentifikasi.';

        if (!is_null($verifyUser)) {
            $partner = $verifyUser->partner;

            if (!$partner->email_verified_at) {
                $verifyUser->partner->email_verified_at = date("Y-m-d H:i:s");
                $verifyUser->partner->save();
                $message = "Email Anda telah diverifikasi. Anda sekarang dapat masuk.";
            } else {
                $message = "Email Anda sudah diverifikasi. Anda sekarang dapat masuk.";
            }
        }
        return redirect()->route('eproc.auth.index')->with('message', $message);
    }
    public function do_logout(){
        $user = Auth::guard('procurement')->user();
        Auth::logout($user);
        return redirect()->route('eproc.auth.index');
    }
}
