<?php

namespace App\Http\Controllers\Eproc;

use App\Models\Tender;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TenderController extends Controller
{
    public function index(Request $request)
    {
        $auth = Auth::guard('procurement')->user();
        if($auth->verified_at){
            if ($request->ajax()) {
                $keywords = $request->keyword;
                $collection = Tender::where('judul','LIKE','%'.$keywords.'%')->where('st','=','Terpublikasi')->orderBy('created_at', 'desc')->paginate(10);
                return view('page.eproc.tender.list', compact('collection'));
            }
            return view('page.eproc.tender.main');
        }else{
            return redirect()->route('eproc.profile.index');
        }
    }
    public function show(Tender $tender)
    {
        return view('page.eproc.tender.show', ['data' => $tender]);
    }
    public function create_penawaran(Tender $tender){
        return view('page.eproc.tender.input', ['data' => $tender]);
    }
    public function download(Tender $tender){
        return Storage::download($tender->attachment);
    }
    public function download_spk(Tender $tender){
        return Storage::download($tender->attachment_spk);
    }
}
