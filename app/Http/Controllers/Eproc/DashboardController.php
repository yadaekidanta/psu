<?php

namespace App\Http\Controllers\Eproc;

use App\Models\Pesan;
use App\Models\Konten;
use App\Models\Tender;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    public function guest(Request $request)
    {
        if ($request->ajax()) {
            $collection = Tender::where('st','=','Terpublikasi')->orderBy('created_at', 'desc')->paginate(10);
            return view('page.eproc.home.list', compact('collection'));
        }
        return view('page.eproc.home.main');
    }
    public function index()
    {
        $auth = Auth::guard('procurement')->user();
        if($auth->verified_at){
            $publikasi = Tender::with('list_penawaran')->where('st','=','Terpublikasi')->get();
            $nilai = 0;
            foreach ($publikasi as $pub){
                $nilai += $pub->list_penawaran->sum('nilai');
            }
            $berjalan = Tender::where('st','=','Berjalan')->with('list_penawaran')->get();
            $selesai = Tender::where('st','=','Selesai')->with('list_penawaran')->get();
            return view('page.eproc.dashboard.main',compact('publikasi','nilai','berjalan','selesai'));
        }else{
            return redirect()->route('eproc.profile.index');
        }
    }
    public function about(){
        $tentang = Konten::where('tipe','=','tentang-eproc')->first();
        return view('page.eproc.tentang.modal',compact('tentang'));
    }
    public function guide(){
        $panduan = Konten::where('tipe','=','panduan-eproc')->first();
        return view('page.eproc.panduan.modal',compact('panduan'));
    }
    public function contact(){
        return view('page.eproc.contact.modal');
    }
    public function kirim_pesan(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required|email',
            'hp' => 'required',
            'subject' => 'required',
            'pesan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('hp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('hp'),
                ]);
            }elseif ($errors->has('subject')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subject'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pesan'),
                ]);
            }
        }
        $request['created_at'] = date("Y-m-d H:i:s");
        $pesan = new Pesan;
        $pesan->nama = $request->nama;
        $pesan->email = $request->email;
        $pesan->hp = $request->hp;
        $pesan->subject = $request->subject;
        $pesan->pesan = $request->pesan;
        $pesan->created_at = date("Y-m-d H:i:s");
        $pesan->save();
        Mail::to($request->email)->send(new ContactMail($pesan));
        return response()->json([
            'alert' => 'success',
            'message' => 'Terima kasih menghubungi kami, kami akan membalasnya secepat mungkin. '. $request->nama,
        ]);
    }
}
