<?php

namespace App\Http\Controllers\Eproc;

use DateTime;
use Carbon\Carbon;
use App\Models\Tender;
use App\Models\Employee;
use App\Models\PartnerType;
use App\Models\TenderOffer;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\NewOffer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class TenderOfferController extends Controller
{
    public function index(Request $request)
    {
        $auth = Auth::guard('procurement')->user();
        if($auth->verified_at){
            if ($request->ajax()) {
                $auth = Auth::guard('procurement')->user();
                $collection = TenderOffer::where('partner_id','=',$auth->id)->orderBy('created_at', 'desc')->paginate(10);
                return view('page.eproc.offer.list', compact('collection'));
            }
            return view('page.eproc.offer.main');
        }else{
            return redirect()->route('eproc.profile.index');
        }
    }
    public function store(Request $request)
    {
        $tender = Tender::where('id','=',$request->tender_id)->first();
        $now = Carbon::parse(date("Y-m-d"));
        $datetime1 = Carbon::parse($tender->tender_deadline_at);
        $diff = $now->lte($datetime1);
        if($diff == true){
            $validator = Validator::make($request->all(), [
                'file' => 'required|mimes:pdf',
                'pesan' => 'required',
                'nilai_penawaran' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                if ($errors->has('file')) {
                    return response()->json([
                        'alert' => 'error',
                        'message' => $errors->first('file'),
                    ]);
                }elseif ($errors->has('pesan')) {
                    return response()->json([
                        'alert' => 'error',
                        'message' => $errors->first('pesan'),
                    ]);
                }elseif ($errors->has('nilai_penawaran')) {
                    return response()->json([
                        'alert' => 'error',
                        'message' => $errors->first('nilai_penawaran'),
                    ]);
                }
            }
            $auth = Auth::guard('procurement')->user();
            $partner_type = PartnerType::where('id','=',Auth::guard('procurement')->user()->partner_type_id)->first();
            $nilai_tender = $tender->nilai_tender;
            // if($partner_type->nilai.$partner_type->simbol.$nilai_tender){
            $cek = TenderOffer::where('tender_id','=',$request->tender_id)->where('partner_id','=',$auth->id)->orderBy('id','DESC')->first();
            if($cek){
                if($cek->st == "Penawaran terkirim"){
                    return response()->json([
                        'alert' => 'info',
                        'message' => 'Anda pernah melakukan penawaran pada tender ini!',
                    ]);
                }
            }
            if(Str::remove(',',$request->nilai_penawaran) > $tender->nilai_tender){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Penawaran anda melebihi nilai tender, harap periksa kembali',
                ]);
            }
            $offer = new TenderOffer;
            if(request()->file('file')){
                $file = request()->file('file')->store("offer");
                $offer->file = $file;
            }
            $offer->tender_id = $request->tender_id;
            $offer->partner_id = $auth->id;
            $offer->pesan = $request->pesan;
            $offer->nilai = Str::remove(',',$request->nilai_penawaran);
            $offer->st = 'Penawaran terkirim';
            $offer->save();
            $employee = Employee::get();
            foreach ($employee AS $emp) {
                Notification::send($emp, new NewOffer($offer->tender,$offer));
            }
            return response()->json([
                'alert' => 'success',
                'message' => 'Penawaran terkirim',
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Pendaftaran telah ditutup',
            ]);
        }
        // }else{
        //     return response()->json([
        //         'alert' => 'info',
        //         'message' => 'Tipe rekanan Anda tidak sesuai dengan tender ini!',
        //     ]);
        // }
    }
    public function show(TenderOffer $tenderOffer)
    {
        return view('page.eproc.offer.main',compact('tenderOffer'));
    }
}
