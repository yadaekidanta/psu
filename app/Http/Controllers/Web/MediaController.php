<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index_berita(Request $request)
    {
        $galeri = Media::where('tipe','=','galeri')->orderBy('created_at', 'DESC')->limit(10)->get();
        $pengumuman = Media::where('tipe','=','pengumuman')->orderBy('created_at', 'DESC')->limit(10)->get();
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Media::where('judul','LIKE','%'.$keywords.'%')
            ->where('tipe','=','berita')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.web.berita.list', compact('collection'));
        }
        return view('page.web.berita.main', compact('galeri','pengumuman'));
    }
    public function berita_show(Media $media)
    {
        $galeri = Media::where('tipe','=','galeri')->orderBy('created_at', 'DESC')->limit(10)->get();
        $pengumuman = Media::where('tipe','=','pengumuman')->orderBy('created_at', 'DESC')->limit(10)->get();
        $related = Media::where('tipe','=','berita')->where('id','!=', $media->id)->limit(4)->get();
        return view('page.web.berita.show', compact('media','related','galeri','pengumuman'));
    }
    public function index_galeri(Request $request)
    {
        $berita = Media::where('tipe','=','berita')->orderBy('created_at', 'DESC')->limit(10)->get();
        $pengumuman = Media::where('tipe','=','pengumuman')->orderBy('created_at', 'DESC')->limit(10)->get();
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Media::where('judul','LIKE','%'.$keywords.'%')
            ->where('tipe','=','galeri')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.web.galeri.list', compact('collection'));
        }
        return view('page.web.galeri.main', compact('berita','pengumuman'));
    }
    public function galeri_show(Media $media)
    {
        $berita = Media::where('tipe','=','berita')->orderBy('created_at', 'DESC')->limit(10)->get();
        $pengumuman = Media::where('tipe','=','pengumuman')->orderBy('created_at', 'DESC')->limit(10)->get();
        $related = Media::where('tipe','=','galeri')->where('id','!=', $media->id)->limit(4)->get();
        return view('page.web.galeri.show', compact('media','related','berita','pengumuman'));
    }
    public function index_pengumuman(Request $request)
    {
        $berita = Media::where('tipe','=','berita')->orderBy('created_at', 'DESC')->limit(10)->get();
        $galeri = Media::where('tipe','=','galeri')->orderBy('created_at', 'DESC')->limit(10)->get();
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Media::where('judul','LIKE','%'.$keywords.'%')
            ->where('tipe','=','pengumuman')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.web.pengumuman.list', compact('collection'));
        }
        return view('page.web.pengumuman.main', compact('berita','galeri'));
    }
    public function pengumuman_show(Media $media)
    {
        $galeri = Media::where('tipe','=','galeri')->orderBy('created_at', 'DESC')->limit(10)->get();
        $berita = Media::where('tipe','=','berita')->orderBy('created_at', 'DESC')->limit(10)->get();
        $related = Media::where('tipe','=','pengumuman')->where('id','!=', $media->id)->limit(4)->get();
        return view('page.web.pengumuman.show', compact('media','related','galeri','berita'));
    }
}
