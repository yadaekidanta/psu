<?php

namespace App\Http\Controllers\Web;

use App\Models\Media;
use App\Models\Pesan;
use App\Models\Konten;
use App\Models\Employee;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RouteController extends Controller
{
    public function index(){
        $collection = Media::where('tipe','=','berita')
        ->orderBy('created_at', 'DESC')
        ->limit(8);
        return view('page.web.home.main',compact('collection'));
    }
    public function highlight(){
        $konten = Konten::where('tipe','=','highlight')->first();
        return view('page.web.highlight.main', compact('konten'));
    }
    public function komisaris_direksi(){
        $komisaris = Employee::where('role_id','=','2')->get();
        $direksi = Employee::where('role_id','=','5')->orWhere('role_id','=','6')->orWhere('role_id','=','7')->get();
        return view('page.web.komisaris_direksi.main', compact('komisaris','direksi'));
    }
    public function struktur_organisasi(){
        $konten = Konten::where('tipe','=','struktur-organisasi')->first();
        return view('page.web.struktur_organisasi.main', compact('konten'));
    }
    public function visi_misi(){
        $konten = Konten::where('tipe','=','visi-misi')->first();
        return view('page.web.visi_misi.main', compact('konten'));
    }
    public function mitra_pelanggan(){
        $konten = Konten::where('tipe','=','mitra-pelanggan')->first();
        return view('page.web.mitra_pelanggan.main', compact('konten'));
    }
    public function sertifikasi_penghargaan(){
        $konten = Konten::where('tipe','=','sertifikasi-penghargaan')->first();
        return view('page.web.sertifikasi_penghargaan.main', compact('konten'));
    }
    public function kontak(){
        return view('page.web.kontak.main');
    }
    public function karir(){
        return abort(404);
    }
    public function kirim_pesan(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required|email',
            'hp' => 'required',
            'subject' => 'required',
            'pesan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('hp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('hp'),
                ]);
            }elseif ($errors->has('subject')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('subject'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pesan'),
                ]);
            }
        }
        $request['created_at'] = date("Y-m-d H:i:s");
        $pesan = new Pesan;
        $pesan->nama = $request->nama;
        $pesan->email = $request->email;
        $pesan->hp = $request->hp;
        $pesan->subject = $request->subject;
        $pesan->pesan = $request->pesan;
        $pesan->created_at = date("Y-m-d H:i:s");
        $pesan->save();
        Mail::to($request->email)->send(new ContactMail($pesan));
        return response()->json([
            'alert' => 'success',
            'message' => 'Terima kasih menghubungi kami, kami akan membalasnya secepat mungkin. '. $request->nama,
        ]);
    }
}
