<?php

namespace App\Http\Controllers\Web;

use App\Models\Laporan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Laporan::where('judul','LIKE','%'.$keywords.'%')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.web.laporan.list', compact('collection'));
        }
        return view('page.web.laporan.main');
    }
    public function show(Laporan $laporan)
    {
        return response()->make(Storage::get($laporan->file),200,['Content-Type' => 'application/pdf']);
    }
}
