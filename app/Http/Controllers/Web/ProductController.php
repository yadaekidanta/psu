<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Product::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.web.product.list', compact('collection'));
        }
        return view('page.web.product.main');
    }
    public function show(Product $product)
    {
        return view('page.web.product.show', compact('product'));
    }
}
