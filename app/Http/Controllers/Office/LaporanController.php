<?php

namespace App\Http\Controllers\Office;

use App\Models\Laporan;
use App\Models\LaporanType;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Laporan::where('judul','LIKE','%'.$keywords.'%')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.laporan.list', compact('collection'));
        }
        return view('page.office.laporan.main');
    }
    public function create()
    {
        $type = LaporanType::get();
        return view('page.office.laporan.input', ['data' => new Laporan, 'type' => $type]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jenis_laporan' => 'required',
            'judul' => 'required|unique:laporan,judul',
            'tahun' => 'required',
            'file' => 'required|mimes:pdf',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jenis_laporan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_laporan'),
                ]);
            }elseif ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('tahun')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tahun'),
                ]);
            }elseif ($errors->has('file')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }
        }
        $data = new Laporan;
        $data->judul = Str::title($request->judul);
        $data->slug = Str::slug($request->judul);
        $data->isi = $request->content;
        if(request()->file('file')){
            $file = request()->file('file')->store("laporan");
            $data->file = $file;
        }
        $data->tahun = $request->tahun;
        $data->laporan_type_id = $request->jenis_laporan;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Laporan tersimpan',
        ]);
    }
    public function show(Laporan $laporan)
    {
        //
    }
    public function edit(Laporan $laporan)
    {
        $type = LaporanType::get();
        return view('page.office.laporan.input', ['data' => $laporan, 'type' => $type]);
    }
    public function update(Request $request, Laporan $laporan)
    {
        $validator = Validator::make($request->all(), [
            'jenis_laporan' => 'required',
            'judul' => 'required|unique:laporan,judul',
            'tahun' => 'required',
            'file' => 'mimes:pdf',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jenis_laporan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_laporan'),
                ]);
            }elseif ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('tahun')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tahun'),
                ]);
            }elseif ($errors->has('file')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }
        }
        $laporan->judul = Str::title($request->judul);
        $laporan->slug = Str::slug($request->judul);
        $laporan->isi = $request->content;
        if(request()->file('file')){
            Storage::delete($laporan->file);
            $file = request()->file('file')->store("laporan");
            $laporan->file = $file;
        }
        $laporan->tahun = $request->tahun;
        $laporan->laporan_type_id = $request->jenis_laporan;
        $laporan->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Laporan terubah',
        ]);
    }
    public function destroy(Laporan $laporan)
    {
        Storage::delete($laporan->file);
        $laporan->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Laporan terhapus',
        ]);
    }
    public function open_file(Laporan $laporan)
    {
        return response()->make(Storage::get($laporan->file),200,['Content-Type' => 'application/pdf']);
    }
    public function download_file(Laporan $laporan)
    {
        return Storage::download($laporan->file);
    }
}