<?php

namespace App\Http\Controllers\Office\Sdm;

use App\Models\Role;
use App\Models\Divisi;
use App\Models\Employee;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Mail\WelcomeMailEmployee;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Employee::where('nama','LIKE','%'.$keywords.'%')
            ->where('id','!=',1)
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.sdm.employee.list', compact('collection'));
        }
        return view('page.office.sdm.employee.main');
    }
    public function create()
    {
        $lokasi = Location::get();
        return view('page.office.sdm.employee.input', ['data' => new Employee, 'lokasi' => $lokasi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lokasi' => 'required',
            'divisi' => 'required',
            'jabatan' => 'required',
            'nama' => 'required',
            'email' => 'required|email|unique:employee,email',
            'hp' => 'required|unique:employee,hp',
            'password' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'tanggal_pengangkatan' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'alamat_domisili' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('lokasi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lokasi'),
                ]);
            }elseif ($errors->has('divisi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('divisi'),
                ]);
            }elseif ($errors->has('jabatan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jabatan'),
                ]);
            }elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('hp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('hp'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }elseif ($errors->has('tempat_lahir')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tempat_lahir'),
                ]);
            }elseif ($errors->has('tanggal_lahir')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_lahir'),
                ]);
            }elseif ($errors->has('tanggal_pengangkatan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_pengangkatan'),
                ]);
            }elseif ($errors->has('jenis_kelamin')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_kelamin'),
                ]);
            }elseif ($errors->has('agama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('agama'),
                ]);
            }elseif ($errors->has('alamat')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif ($errors->has('alamat_domisili')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat_domisili'),
                ]);
            }
        }
        $data = new Employee;
        $data->lokasi_id = $request->lokasi;
        $data->divisi_id = $request->divisi;
        $data->role_id = $request->jabatan;
        $data->nip = $request->nip;
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->hp = $request->hp;
        $data->password = Hash::make($request->password);
        $data->tempat_lahir = $request->tempat_lahir;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->tanggal_pengangkatan = $request->tanggal_pengangkatan;
        $data->jk = $request->jenis_kelamin;
        $data->agama = $request->agama;
        $data->alamat = $request->alamat;
        $data->alamat_domisili = $request->alamat_domisili;
        $data->save();
        Mail::to($data->email)->send(new WelcomeMailEmployee($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'Pegawai tersimpan',
        ]);
    }
    public function show(Employee $employee)
    {
        //
    }
    public function edit(Employee $employee)
    {
        //
    }
    public function update(Request $request, Employee $employee)
    {
        //
    }
    public function destroy(Employee $employee)
    {
        $employee->delete();
    }
}
