<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Karir;
use Illuminate\Http\Request;

class KarirController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Karir $karir)
    {
        //
    }
    public function edit(Karir $karir)
    {
        //
    }
    public function update(Request $request, Karir $karir)
    {
        //
    }
    public function destroy(Karir $karir)
    {
        //
    }
}
