<?php

namespace App\Http\Controllers\Office\Pengaturan\Web;

use App\Models\MediaGaleri;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Media AS Pengumuman;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PengumumanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Pengumuman::where('judul','LIKE','%'.$keywords.'%')
            ->where('tipe','=','pengumuman')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.office.pengaturan.web.media.pengumuman.list', compact('collection'));
        }
        return view('page.office.pengaturan.web.media.pengumuman.main');
    }
    public function create()
    {
        return view('page.office.pengaturan.web.media.pengumuman.input', ['data' => new Pengumuman]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|unique:media,judul',
            'content' => 'required',
            'thumbnail' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }
        }
        $data = new Pengumuman;
        $data->judul = Str::title($request->judul);
        $data->slug = Str::slug($request->judul);
        $data->isi = $request->content;
        if(request()->file('thumbnail')){
            $thumbnail = request()->file('thumbnail')->store("media");
            $data->thumbnail = $thumbnail;
        }
        $data->tipe = 'pengumuman';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengumuman tersimpan',
        ]);
    }
    public function show(Request $request, Pengumuman $pengumuman)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = MediaGaleri::where('media_id','=',$pengumuman->id)
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.pengaturan.web.media.media.list', ['collection' => $collection, 'media' => $pengumuman]);
        }
        return view('page.office.pengaturan.web.media.pengumuman.show', compact('pengumuman'));
    }
    public function edit(Pengumuman $pengumuman)
    {
        return view('page.office.pengaturan.web.media.pengumuman.input', ['data' => $pengumuman]);
    }
    public function update(Request $request, Pengumuman $pengumuman)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }
        }
        $pengumuman->judul = Str::title($request->judul);
        $pengumuman->isi = $request->content;
        if(request()->file('thumbnail')){
            Storage::delete($pengumuman->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("media");
            $pengumuman->thumbnail = $thumbnail;
        }
        $pengumuman->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengumuman terubah',
        ]);
    }
    public function destroy(Pengumuman $pengumuman)
    {
        $pengumuman = MediaGaleri::where('media_id','=',$pengumuman->id)->get();
        foreach($pengumuman as $glr){
            Storage::delete($glr->gambar);
        }
        MediaGaleri::where('media_id','=',$pengumuman->id)->delete();
        Storage::delete($pengumuman->thumbnail);
        $pengumuman->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengumuman terhapus',
        ]);
    }
}
