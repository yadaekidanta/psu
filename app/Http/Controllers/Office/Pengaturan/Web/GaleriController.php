<?php

namespace App\Http\Controllers\Office\Pengaturan\Web;

use App\Models\Media AS Galeri;
use App\Models\MediaGaleri;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GaleriController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Galeri::where('judul','LIKE','%'.$keywords.'%')
            ->where('tipe','=','galeri')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.office.pengaturan.web.media.galeri.list', compact('collection'));
        }
        return view('page.office.pengaturan.web.media.galeri.main');
    }
    public function create()
    {
        return view('page.office.pengaturan.web.media.galeri.input', ['data' => new Galeri]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|unique:media,judul',
            'content' => 'required',
            'thumbnail' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }
        }
        $data = new Galeri;
        $data->judul = Str::title($request->judul);
        $data->slug = Str::slug($request->judul);
        $data->isi = $request->content;
        if(request()->file('thumbnail')){
            $thumbnail = request()->file('thumbnail')->store("media");
            $data->thumbnail = $thumbnail;
        }
        $data->tipe = 'galeri';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Galeri tersimpan',
        ]);
    }
    public function show(Request $request, Galeri $galeri)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = MediaGaleri::where('media_id','=',$galeri->id)
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.pengaturan.web.media.media.list', ['collection' => $collection, 'media' => $galeri]);
        }
        return view('page.office.pengaturan.web.media.galeri.show', compact('galeri'));
    }
    public function edit(Galeri $galeri)
    {
        return view('page.office.pengaturan.web.media.galeri.input', ['data' => $galeri]);
    }
    public function update(Request $request, Galeri $galeri)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }
        }
        $galeri->judul = Str::title($request->judul);
        $galeri->isi = $request->content;
        if(request()->file('thumbnail')){
            Storage::delete($galeri->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("media");
            $galeri->thumbnail = $thumbnail;
        }
        $galeri->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Galeri terubah',
        ]);
    }
    public function destroy(Galeri $galeri)
    {
        $galeri = MediaGaleri::where('media_id','=',$galeri->id)->get();
        foreach($galeri as $glr){
            Storage::delete($glr->gambar);
        }
        MediaGaleri::where('media_id','=',$galeri->id)->delete();
        Storage::delete($galeri->thumbnail);
        $galeri->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Galeri terhapus',
        ]);
    }
}
