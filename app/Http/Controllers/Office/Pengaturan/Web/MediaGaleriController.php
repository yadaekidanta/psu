<?php

namespace App\Http\Controllers\Office\Pengaturan\Web;

use App\Models\Media;
use App\Models\MediaGaleri;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MediaGaleriController extends Controller
{
    public function index()
    {
        //
    }
    public function create(Media $media)
    {
        return view('page.office.pengaturan.web.media.media.input', ['data' => new MediaGaleri, 'media' => $media]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gambar' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('gambar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gambar'),
                ]);
            }
        }
        $data = new MediaGaleri;
        if(request()->file('gambar')){
            $gambar = request()->file('gambar')->store("galeri");
            $data->gambar = $gambar;
        }
        $data->media_id = $request->media_id;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Galeri tersimpan',
        ]);
    }
    public function show(MediaGaleri $mediaGaleri)
    {
        //
    }
    public function edit(Media $media,MediaGaleri $mediaGaleri)
    {
        return view('page.office.pengaturan.web.media.media.input', ['data' => $mediaGaleri, 'media' => $media]);
    }
    public function update(Request $request, MediaGaleri $mediaGaleri)
    {
        $validator = Validator::make($request->all(), [
            'gambar' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('gambar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gambar'),
                ]);
            }
        }
        if(request()->file('gambar')){
            Storage::delete($mediaGaleri->gambar);
            $gambar = request()->file('gambar')->store("galeri");
            $mediaGaleri->gambar = $gambar;
        }
        $mediaGaleri->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Galeri tersimpan',
        ]);
    }
    public function destroy(MediaGaleri $mediaGaleri)
    {
        Storage::delete($mediaGaleri->gambar);
        $mediaGaleri->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Galeri terhapus',
        ]);
    }
}
