<?php

namespace App\Http\Controllers\Office\Pengaturan\Web;

use App\Models\Media AS Beritum;
use App\Models\MediaGaleri;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BeritaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Beritum::where('judul','LIKE','%'.$keywords.'%')
            ->where('tipe','=','berita')
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
            return view('page.office.pengaturan.web.media.berita.list', compact('collection'));
        }
        return view('page.office.pengaturan.web.media.berita.main');
    }
    public function create()
    {
        return view('page.office.pengaturan.web.media.berita.input', ['data' => new Beritum]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|unique:media,judul',
            'content' => 'required',
            'thumbnail' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }
        }
        $data = new Beritum;
        $data->judul = Str::title($request->judul);
        $data->slug = Str::slug($request->judul);
        $data->isi = $request->content;
        if(request()->file('thumbnail')){
            $thumbnail = request()->file('thumbnail')->store("media");
            $data->thumbnail = $thumbnail;
        }
        $data->tipe = 'berita';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Berita tersimpan',
        ]);
    }
    public function show(Request $request, Beritum $beritum)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = MediaGaleri::where('media_id','=',$beritum->id)
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.pengaturan.web.media.media.list', ['collection' => $collection, 'media' => $beritum]);
        }
        return view('page.office.pengaturan.web.media.berita.show', compact('beritum'));
    }
    public function edit(Beritum $beritum)
    {
        return view('page.office.pengaturan.web.media.berita.input', ['data' => $beritum]);
    }
    public function update(Request $request, Beritum $beritum)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('thumbnail')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('thumbnail'),
                ]);
            }
        }
        $beritum->judul = Str::title($request->judul);
        $beritum->isi = $request->content;
        if(request()->file('thumbnail')){
            Storage::delete($beritum->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("media");
            $beritum->thumbnail = $thumbnail;
        }
        $beritum->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Berita terubah',
        ]);
    }
    public function destroy(Beritum $beritum)
    {
        $galeri = MediaGaleri::where('media_id','=',$beritum->id)->get();
        foreach($galeri as $glr){
            Storage::delete($glr->gambar);
        }
        MediaGaleri::where('media_id','=',$beritum->id)->delete();
        Storage::delete($beritum->thumbnail);
        $beritum->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Berita terhapus',
        ]);
    }
}