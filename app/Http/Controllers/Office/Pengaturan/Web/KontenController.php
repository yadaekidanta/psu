<?php

namespace App\Http\Controllers\Office\Pengaturan\Web;

use App\Http\Controllers\Controller;
use App\Models\Konten;
use Illuminate\Http\Request;

class KontenController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Konten::where('isi','LIKE','%'.$keywords.'%')
            ->where('id','<','6')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.pengaturan.web.konten.list', compact('collection'));
        }
        return view('page.office.pengaturan.web.konten.main');
    }
    public function index_eproc(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Konten::where('isi','LIKE','%'.$keywords.'%')
            ->where('id','>','5')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.pengaturan.eproc.konten.list', compact('collection'));
        }
        return view('page.office.pengaturan.eproc.konten.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Konten $konten)
    {
        //
    }
    public function edit(Konten $konten)
    {
        return view('page.office.pengaturan.web.konten.input', ['data' => $konten]);
    }
    public function update(Request $request, Konten $konten)
    {
        $konten->isi = $request->content;
        $konten->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Konten tersimpan',
        ]);
    }
    public function edit_eproc(Konten $konten)
    {
        return view('page.office.pengaturan.eproc.konten.input', ['data' => $konten]);
    }
    public function update_eproc(Request $request, Konten $konten)
    {
        $konten->isi = $request->content;
        $konten->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Konten tersimpan',
        ]);
    }
    public function destroy(Konten $konten)
    {
        //
    }
}
