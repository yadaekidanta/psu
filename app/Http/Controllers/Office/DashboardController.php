<?php

namespace App\Http\Controllers\Office;

use App\Models\Tender;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $publikasi = Tender::with('list_penawaran')->where('st','=','Terpublikasi')->get();
        $nilai = 0;
        foreach ($publikasi as $pub){
            $nilai += $pub->list_penawaran->sum('nilai');
        }
        $berjalan = Tender::where('st','=','Berjalan')->with('list_penawaran')->get();
        $selesai = Tender::where('st','=','Selesai')->with('list_penawaran')->get();
        return view('page.office.dashboard.main',compact('publikasi','nilai','berjalan','selesai'));
    }
}