<?php

namespace App\Http\Controllers\Office;

use App\Models\Pesan;
use Illuminate\Http\Request;
use App\Mail\ReplyContactMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PesanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Pesan::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
            return view('page.office.pesan.list', compact('collection'));
        }
        return view('page.office.pesan.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pesan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('pesan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pesan'),
                ]);
            }
        }
        $data = [
            'nama' => $request->nama,
            'email' => $request->email,
            'subject' => $request->subject,
            'pesan' => $request->pesan
        ];
        Mail::to($request->email)->send(new ReplyContactMail($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'Balasan terkirim',
        ]);
    }
    public function show(Pesan $pesan)
    {
        //
    }
    public function edit(Pesan $pesan)
    {
        return view('page.office.pesan.input', ['data' => $pesan]);
    }
    public function update(Request $request, Pesan $pesan)
    {
        //
    }
    public function destroy(Pesan $pesan)
    {
        //
    }
}
