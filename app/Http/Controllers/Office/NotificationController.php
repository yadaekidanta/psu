<?php

namespace App\Http\Controllers\Office;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function counter(Request $request){
        $partner = Employee::where('id','=',Auth::guard('office')->user()->id)->first();
        return response()->json([
            'total_notif' => $partner->unreadNotifications->count(),
        ]);
    }
    public function index(Request $request){
        $output = '';
        $notif = '';
        $simbol = '';
        $partner = Employee::where('id','=',Auth::guard('office')->user()->id)->first();
        if($partner->notifications->count() > 0){
            foreach ($partner->notifications as $notification) {
                $simbol = '
                    <span class="symbol-label bg-light-success">
                        <span class="svg-icon svg-icon-2 svg-icon-success">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="black" />
                                <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="black" />
                            </svg>
                        </span>
                    </span>
                    ';
                    $notif = '
                    <a href="javascript:;" class="fs-6 text-gray-800 text-hover-primary fw-bolder">'.$notification->data['judul'].'</a>
                    <div class="text-gray-400 fs-7">'.$notification->data['partner'].' telah mengirimkan penawaran dengan nilai Rp. '.number_format($notification->data['nilai_penawaran']).'</div>
                ';
                $output .= '
                <div class="d-flex flex-stack py-4">
                    <div class="d-flex align-items-center">
                        <div class="symbol symbol-35px me-4">
                            '.$simbol.'
                        </div>
                        <div class="mb-0 me-2">
                            '.$notif.'                            
                        </div>
                    </div>
                    <span class="badge badge-light fs-8">'.$notification->created_at->diffForHumans().'</span>
                </div>
                ';
            }
        }else{
            $output = '
            <div class="d-flex flex-column px-9">
                <div class="pt-10 pb-0">
                    <h3 class="text-dark text-center fw-bolder">Belum ada pemberitahuan</h3>
                    <div class="text-center text-gray-600 fw-bold pt-1">Tetap pantau terus tendernya ya...</div>
                    <div class="text-center mt-5 mb-9 d-none">
                        <a href="#" class="btn btn-sm btn-primary px-6" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">Upgrade</a>
                    </div>
                </div>
                <div class="text-center px-4">
                    <img class="mw-100 mh-200px" alt="image" src="'.asset('keenthemes/media/illustrations/sigma-1/1.png').'" />
                </div>
            </div>
            ';
        }
        $partner->unreadNotifications->markAsRead();
        return response()->json([
            'collection' => $output,
            'total_notif' => $partner->unreadNotifications->count(),
        ]);
    }
}
