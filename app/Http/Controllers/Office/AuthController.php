<?php

namespace App\Http\Controllers\Office;

use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\PasswordChangedMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\ConfirmationResetPasswordMail;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:office')->except('do_logout');
    }
    public function index()
    {
        return view('page.office.auth.main');
    }
    public function do_login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:employee|email|max:255',
            'password' => 'required|min:8',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        if(Auth::guard('office')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return response()->json([
                'alert' => 'success',
                'message' => 'Selamat datang '. Auth::guard('office')->user()->nama,
                'callback' => 'reload',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.',
            ]);
        }
    }
    public function do_forgot(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:employee',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
        }
        $employee = Employee::where('email',$request->email)->first();

        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        $data = array(
            'token' => $token,
            'employee' => $employee
        );
        Mail::to($request->email)->send(new ConfirmationResetPasswordMail($data));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kami telah mengirim email tautan pemulihan kata sandi Anda!',
            'callback' => 'page_login',
        ]);
    }
    public function reset($token){
        $updatePassword = DB::table('password_resets')->where(['token' => $token])->first();
        if (!$updatePassword) {
            return redirect()->route('auth.index');
        }else{
            return view('page.office.auth.reset', compact('token'));
        }
    }
    public function do_reset(Request $request){
        $validator = Validator::make($request->all(), [
            'token' => 'required|exists:password_resets',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('token')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('token'),
                ]);
            } elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            } elseif ($errors->has('password_confirmation')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password_confirmation'),
                ]);
            }
        }
        $updatePassword = DB::table('password_resets')->where(['token' => $request->token])->first();
        if (!$updatePassword) {
            return response()->json([
                'alert' => 'error',
                'message' => 'Invalid token!',
            ]);
        }
        Employee::where('email', $updatePassword->email)->update(['password' => Hash::make($request->password)]);
        $users = Employee::where('email', $updatePassword->email)->first();
        DB::table('password_resets')->where(['email' => $updatePassword->email])->delete();
        Mail::to($users->email)->send(new PasswordChangedMail($users));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kata sandi Anda telah diubah!',
            'callback' => 'reload',
        ]);
    }
    public function do_logout(){
        $user = Auth::guard('office')->user();
        Auth::logout($user);
        return redirect()->route('office.auth.index');
    }
}