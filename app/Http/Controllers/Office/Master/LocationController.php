<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Employee;
use App\Models\Location;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Divisi;
use App\Models\Role;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Location::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.master.location.list', compact('collection'));
        }
        return view('page.office.master.location.main');
    }
    public function create()
    {
        return view('page.office.master.location.input', ['data' => new Location]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:lokasi,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new Location;
        $data->nama = Str::title($request->nama);
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lokasi tersimpan',
        ]);
    }
    public function show(Location $location)
    {
        //
    }
    public function edit(Location $location)
    {
        return view('page.office.master.location.input', ['data' => $location]);
    }
    public function update(Request $request, Location $location)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:lokasi,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $location->nama = Str::title($request->nama);
        $location->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lokasi terubah',
        ]);
    }
    public function destroy(Location $location)
    {
        Employee::where('lokasi_id', '=', $location->id)->delete();
        Role::where('lokasi_id', '=', $location->id)->delete();
        Divisi::where('lokasi_id', '=', $location->id)->delete();
        $location->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lokasi terhapus',
        ]);
    }
}
