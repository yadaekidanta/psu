<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Role;
use App\Models\Divisi;
use App\Models\Employee;
use App\Models\Location;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DivisiController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Divisi::where('nama','LIKE','%'.$keywords.'%')
            ->where('id','!=','1')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.master.divisi.list', compact('collection'));
        }
        return view('page.office.master.divisi.main');
    }
    public function create()
    {
        $lokasi = Location::get();
        return view('page.office.master.divisi.input', ['data' => new Divisi, 'lokasi' => $lokasi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lokasi' => 'required',
            'nama' => 'required|unique:divisi,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('lokasi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lokasi'),
                ]);
            }elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new Divisi;
        $data->nama = Str::title($request->nama);
        $data->lokasi_id = $request->lokasi;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Divisi tersimpan',
        ]);
    }
    public function show(Divisi $divisi)
    {
        //
    }
    public function edit(Divisi $divisi)
    {
        $lokasi = Location::get();
        return view('page.office.master.divisi.input', ['data' => $divisi, 'lokasi' => $lokasi]);
    }
    public function update(Request $request, Divisi $divisi)
    {
        $validator = Validator::make($request->all(), [
            'lokasi' => 'required',
            'nama' => 'required|unique:divisi,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('lokasi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lokasi'),
                ]);
            }elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $divisi->nama = Str::title($request->nama);
        $divisi->lokasi_id = $request->lokasi;
        $divisi->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Divisi terubah',
        ]);
    }
    public function destroy(Divisi $divisi)
    {
        Employee::where('divisi_id', '=', $divisi->id)->delete();
        Role::where('divisi_id', '=', $divisi->id)->delete();
        $divisi->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Divisi terhapus',
        ]);
    }
    public function list_option(Request $request){
        $result = Divisi::where('lokasi_id','=',$request->lokasi)->where('id','!=','1')->get();
        $list = "<option value=''>Pilih Divisi</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->nama</option>";
        }
        return $list;
    }

}
