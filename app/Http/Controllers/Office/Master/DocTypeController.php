<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\DocType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DocTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = DocType::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('nama', 'ASC')
            ->paginate(10);
            return view('page.office.master.doc_type.list', compact('collection'));
        }
        return view('page.office.master.doc_type.main');
    }
    public function create()
    {
        return view('page.office.master.doc_type.input', ['data' => new DocType]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new DocType;
        $data->nama = $request->nama;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Dokumen tersimpan',
        ]);
    }
    public function show(DocType $docType)
    {
        //
    }
    public function edit(DocType $docType)
    {
        return view('page.office.master.doc_type.input', ['data' => $docType]);
    }
    public function update(Request $request, DocType $docType)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $docType->nama = $request->nama;
        $docType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Dokumen terupdate',
        ]);
    }
    public function destroy(DocType $docType)
    {
        $docType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Dokumen terhapus',
        ]);
    }
}
