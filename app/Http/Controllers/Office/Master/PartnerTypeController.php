<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\PartnerType;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PartnerTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = PartnerType::where('nama','LIKE','%'.$keywords.'%')->orderBy('nama', 'asc')->paginate(10);
            return view('page.office.master.partner_type.list', compact('collection'));
        }
        return view('page.office.master.partner_type.main');
    }
    public function create()
    {
        return view('page.office.master.partner_type.input',['data' => new PartnerType]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:partner_type,nama',
            'simbol' => 'required',
            'nilai' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
            elseif ($errors->has('simbol')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('simbol'),
                ]);
            }
            elseif ($errors->has('nilai')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }
        }
        $data = new PartnerType;
        $data->nama = $request->nama;
        $data->simbol = $request->simbol;
        $data->nilai = Str::remove(',',$request->nilai);
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Rekanan tersimpan',
        ]);
    }
    public function show(PartnerType $partnerType)
    {
        // return view('page.office.master.partner_type.input',['data' => new PartnerType]);
    }
    public function edit(PartnerType $partnerType)
    {
        return view('page.office.master.partner_type.input',['data' => $partnerType]);
    }
    public function update(Request $request, PartnerType $partnerType)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'simbol' => 'required',
            'nilai' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
            elseif ($errors->has('simbol')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('simbol'),
                ]);
            }
            elseif ($errors->has('nilai')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }
        }
        $partnerType->nama = $request->nama;
        $partnerType->simbol = $request->simbol;
        $partnerType->nilai = Str::remove(',',$request->nilai);
        $partnerType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Rekanan terupdate',
        ]);
    }
    public function destroy(PartnerType $partnerType)
    {
        $partnerType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Rekanan terhapus',
        ]);
    }
}
