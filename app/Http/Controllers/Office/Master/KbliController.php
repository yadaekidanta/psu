<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Kbli;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class KbliController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required|unique:kbli,kode',
            'nama' => 'required|unique:kbli,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('kode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode'),
                ]);
            }
            elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new Kbli;
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $data->jenis_kbli_id = $request->jenis_kbli_id;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'KBLI tersimpan',
        ]);
    }
    public function show(Kbli $kbli)
    {
        //
    }
    public function edit(Kbli $kbli)
    {
        return view('page.office.master.kbli.input',['data' => $kbli]);
    }
    public function update(Request $request, Kbli $kbli)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('kode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode'),
                ]);
            }
            elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $kbli->kode = $request->kode;
        $kbli->nama = $request->nama;
        $kbli->jenis_kbli_id = $request->jenis_kbli_id;
        $kbli->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'KBLI terupdate',
        ]);
    }
    public function destroy(Kbli $kbli)
    {
        //
    }
}
