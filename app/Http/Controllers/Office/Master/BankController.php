<?php

namespace App\Http\Controllers\Office\Master;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Bank::where('nama','LIKE','%'.$keywords.'%')
            ->where('id','!=','1')
            ->orderBy('nama', 'ASC')
            ->paginate(10);
            return view('page.office.master.bank.list', compact('collection'));
        }
        return view('page.office.master.bank.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Bank $bank)
    {
        //
    }
    public function edit(Bank $bank)
    {
        //
    }
    public function update(Request $request, Bank $bank)
    {
        //
    }
    public function destroy(Bank $bank)
    {
        //
    }
}
