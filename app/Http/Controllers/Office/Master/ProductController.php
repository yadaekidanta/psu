<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Product::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.master.product.list', compact('collection'));
        }
        return view('page.office.master.product.main');
    }
    public function create()
    {
        return view('page.office.master.product.input', ['data' => new Product]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:product,nama',
            'content' => 'required',
            'gambar' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }elseif ($errors->has('gambar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('gambar'),
                ]);
            }
        }
        $data = new Product;
        if(request()->file('gambar')){
            $gambar = request()->file('gambar')->store("product");
            $data->gambar = $gambar;
        }
        $data->nama = Str::title($request->nama);
        $data->deskripsi = $request->content;
        $data->slug = Str::slug($request->nama);
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk tersimpan',
        ]);
    }
    public function show(Product $product)
    {
        //
    }
    public function edit(Product $product)
    {
        return view('page.office.master.product.input', ['data' => $product]);
    }
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }
        }
        if(request()->file('gambar')){
            Storage::delete($product->gambar);
            $gambar = request()->file('gambar')->store("product");
            $product->gambar = $gambar;
        }
        $product->nama = Str::title($request->nama);
        $product->slug = Str::slug($request->nama);
        $product->deskripsi = $request->content;
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terubah',
        ]);
    }
    public function destroy(Product $product)
    {
        Storage::delete($product->gambar);
        $product->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terhapus',
        ]);
    }
    public function active(Product $product){
        $product->publish = "y";
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk berhasil ditampilkan',
        ]);
    }
    public function non_active(Product $product){
        $product->publish = "n";
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk berhasil disembunyikan',
        ]);
    }
}
