<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Kbli;
use App\Models\JenisKbli;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JenisKbliController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = JenisKbli::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('nama', 'ASC')
            ->paginate(10);
            return view('page.office.master.jenis_kbli.list', compact('collection'));
        }
        return view('page.office.master.jenis_kbli.main');
    }
    public function index_kbli(JenisKbli $jenisKbli, Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Kbli::where('nama','LIKE','%'.$keywords.'%')
            ->where('jenis_kbli_id','=',$jenisKbli->id)
            ->orderBy('kode', 'ASC')
            ->paginate(10);
            return view('page.office.master.kbli.list', compact('collection'));
        }
        return view('page.office.master.kbli.main',compact('jenisKbli'));
    }
    public function create()
    {
        //
    }
    public function create_kbli(JenisKbli $jenisKbli){
        return view('page.office.master.kbli.input', ['data' => new Kbli, 'jenis' => $jenisKbli]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(JenisKbli $jenisKbli)
    {
        //
    }
    public function edit(JenisKbli $jenisKbli)
    {
        //
    }
    public function update(Request $request, JenisKbli $jenisKbli)
    {
        //
    }
    public function destroy(JenisKbli $jenisKbli)
    {
        //
    }
}
