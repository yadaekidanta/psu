<?php

namespace App\Http\Controllers\Office\Master;

use App\Models\Role;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Role::where('nama','LIKE','%'.$keywords.'%')
            ->where('divisi_id','!=','1')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.master.role.list', compact('collection'));
        }
        return view('page.office.master.role.main');
    }
    public function create()
    {
        $lokasi = Location::get();
        return view('page.office.master.role.input', ['data' => new Role, 'lokasi' => $lokasi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lokasi' => 'required',
            'divisi' => 'required',
            'nama' => 'required|unique:divisi,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('lokasi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lokasi'),
                ]);
            }elseif ($errors->has('divisi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('divisi'),
                ]);
            }elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new Role;
        $data->nama = Str::title($request->nama);
        $data->lokasi_id = $request->lokasi;
        $data->divisi_id = $request->divisi;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jabatan tersimpan',
        ]);
    }
    public function show(Role $role)
    {
        //
    }
    public function edit(Role $role)
    {
        $lokasi = Location::get();
        return view('page.office.master.role.input', ['data' => $role, 'lokasi' => $lokasi]);
    }
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(), [
            'lokasi' => 'required',
            'divisi' => 'required',
            'nama' => 'required|unique:divisi,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('lokasi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lokasi'),
                ]);
            }elseif ($errors->has('divisi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('divisi'),
                ]);
            }elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $role->nama = Str::title($request->nama);
        $role->lokasi_id = $request->lokasi;
        $role->divisi_id = $request->divisi;
        $role->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jabatan terubah',
        ]);
    }
    public function destroy(Role $role)
    {
        Employee::where('role_id', '=', $role->id)->delete();
        $role->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jabatan terhapus',
        ]);
    }
    public function list_option(Request $request){
        $result = Role::where('divisi_id','=',$request->divisi)->where('id','!=','1')->get();
        $list = "<option value=''>Pilih Jabatan</option>";
        foreach($result as $row){
            $list.="<option value='$row->id'>$row->nama</option>";
        }
        return $list;
    }
}
