<?php

namespace App\Http\Controllers\Office;

use App\Models\LaporanType;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Laporan;
use Illuminate\Support\Facades\Validator;

class LaporanTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = LaporanType::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.office.laporanType.list', compact('collection'));
        }
        return view('page.office.laporanType.main');
    }
    public function create()
    {
        return view('page.office.laporanType.input', ['data' => new LaporanType]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:laporan_type,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new LaporanType;
        $data->nama = Str::title($request->nama);
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Laporan tersimpan',
        ]);
    }
    public function show(LaporanType $laporanType)
    {
        //
    }
    public function edit(LaporanType $laporanType)
    {
        return view('page.office.laporanType.input', ['data' => $laporanType]);
    }
    public function update(Request $request, LaporanType $laporanType)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|unique:laporan_type,nama',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $laporanType->nama = Str::title($request->nama);
        $laporanType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Laporan terubah',
        ]);
    }
    public function destroy(LaporanType $laporanType)
    {
        Laporan::where('laporan_type_id', '=', $laporanType->id)->delete();
        $laporanType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Laporan terhapus',
        ]);
    }
}
