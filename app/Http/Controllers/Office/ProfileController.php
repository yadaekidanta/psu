<?php

namespace App\Http\Controllers\Office;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Mail\PasswordChangedMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index(){
        return view('page.office.profile.main');
    }
    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(5)->get()->toArray();
    }
    public function edit(){
        return view('page.office.profile.edit');
    }
    public function change(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'hp' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('hp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('hp'),
                ]);
            }
        }
        $employee = Employee::where('id','=',Auth::guard('office')->user()->id)->first();
        $employee->email = $request->email;
        $employee->hp = $request->hp;
        if(request()->file('avatar')){
            Storage::delete($employee->avatar);
            $avatar = request()->file('avatar')->store("avatar");
            $employee->avatar = $avatar;
        };
        $employee->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profil tersimpan',
            'redirect' => 'reload',
        ]);
    }
    public function edit_password(){
        return view('page.office.profile.edit_password');
    }
    public function change_password(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            } elseif ($errors->has('password_confirmation')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password_confirmation'),
                ]);
            }
        }
        Employee::where('id', Auth::guard('office')->user()->id)->update(['password' => Hash::make($request->password)]);
        $users = Employee::where('id', Auth::guard('office')->user()->id)->first();
        Mail::to($users->email)->send(new PasswordChangedMail($users));
        return response()->json([
            'alert' => 'success',
            'message' => 'Kata sandi tersimpan',
            'redirect' => 'reload',
        ]);
    }
}
