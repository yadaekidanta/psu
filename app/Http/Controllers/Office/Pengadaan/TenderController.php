<?php

namespace App\Http\Controllers\Office\Pengadaan;

use App\Models\Kbli;
use App\Models\Tender;
use App\Models\Partner;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\NewTender;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class TenderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Tender::where('judul','LIKE','%'.$keywords.'%')->orderBy('created_at', 'desc')->paginate(10);
            return view('page.office.tender.list', compact('collection'));
        }
        return view('page.office.tender.main');
    }
    public function create()
    {
        return view('page.office.tender.input', ['data' => new Tender]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'attachment' => 'required|mimes:pdf',
            'nilai_tender' => 'required',
            'tender_deadline' => 'required',
            'work_deadline' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('attachment')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('attachment'),
                ]);
            }elseif ($errors->has('nilai_tender')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai_tender'),
                ]);
            }elseif ($errors->has('tender_deadline')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tender_deadline'),
                ]);
            }elseif ($errors->has('work_deadline')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('work_deadline'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }
        }
        $data = new Tender;
        $data->judul = $request->judul;
        $data->slug = Str::slug($request->judul);
        $data->nilai_tender = Str::remove(',',$request->nilai_tender);
        $data->tender_deadline_at = $request->tender_deadline;
        $data->work_deadline_at = $request->work_deadline;
        $data->deskripsi = $request->content;
        if(request()->file('attachment')){
            $attachment = request()->file('attachment')->store("kak");
            $data->attachment = $attachment;
        }
        $data->st = 'Draft';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tender tersimpan',
        ]);
    }
    public function show(Tender $tender)
    {
        return view('page.office.tender.show', ['data' => $tender]);
    }
    public function show_penawaran(Tender $tender)
    {
        $kbli = Kbli::get();
        return view('page.office.tender.show', ['data' => $tender, 'kbli' => $kbli]);
    }
    public function edit(Tender $tender)
    {
        return view('page.office.tender.input', ['data' => $tender]);
    }
    public function update(Request $request, Tender $tender)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'attachment' => 'mimes:pdf',
            'nilai_tender' => 'required',
            'tender_deadline' => 'required',
            'work_deadline' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('judul')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif ($errors->has('attachment')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('attachment'),
                ]);
            }elseif ($errors->has('nilai_tender')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai_tender'),
                ]);
            }elseif ($errors->has('tender_deadline')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tender_deadline'),
                ]);
            }elseif ($errors->has('work_deadline')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('work_deadline'),
                ]);
            }elseif ($errors->has('content')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('content'),
                ]);
            }
        }
        $tender->judul = $request->judul;
        $tender->slug = Str::slug($request->judul);
        $tender->nilai_tender = Str::remove(',',$request->nilai_tender);
        $tender->tender_deadline_at = $request->tender_deadline;
        $tender->work_deadline_at = $request->work_deadline;
        $tender->deskripsi = $request->content;
        if(request()->file('attachment')){
            Storage::delete($tender->file);
            $attachment = request()->file('attachment')->store("kak");
            $tender->attachment = $attachment;
        }
        $tender->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tender terubah',
        ]);
    }
    public function update_spk(Request $request, Tender $tender)
    {
        $validator = Validator::make($request->all(), [
            'file_spk' => 'mimes:pdf',
            'no_spk' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('file_spk')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file_spk'),
                ]);
            }elseif ($errors->has('no_spk')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_spk'),
                ]);
            }
        }
        $tender->no_spk = $request->no_spk;
        $tender->contract_at = date("Y-m-d");
        if(request()->file('file_spk')){
            Storage::delete($tender->attachment_spk);
            $file_spk = request()->file('file_spk')->store("spk");
            $tender->attachment_spk = $file_spk;
        }
        $tender->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'SPK terupload',
        ]);
    }
    public function destroy(Tender $tender)
    {
        Storage::delete($tender->file);
        $tender->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tender terhapus',
        ]);
    }
    public function publish(Tender $tender){
        $partner = Partner::get();
        foreach ($partner AS $part) {
            Notification::send($part, new NewTender($tender));
        }
        $tender->st = "Terpublikasi";
        $tender->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tender terpublikasi',
        ]);
    }
    public function selesai(Tender $tender){
        $tender->st = "Selesai";
        $tender->end_at = date("Y-m-d");
        $tender->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Tender selesai',
        ]);
    }
}