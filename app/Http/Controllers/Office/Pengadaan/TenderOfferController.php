<?php

namespace App\Http\Controllers\Office\Pengadaan;

use App\Models\Tender;
use App\Models\TenderOffer;
use Illuminate\Http\Request;
use App\Notifications\OfferRevisi;
use App\Http\Controllers\Controller;
use App\Notifications\OfferAccepted;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;

class TenderOfferController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(TenderOffer $tenderOffer)
    {
        //
    }
    public function edit(TenderOffer $tenderOffer)
    {
        //
    }
    public function update(Request $request, TenderOffer $tenderOffer)
    {
        //
    }
    public function destroy(TenderOffer $tenderOffer)
    {
        //
    }
    public function download(TenderOffer $tenderOffer)
    {
        return Storage::download($tenderOffer->file);
    }
    public function revisi(TenderOffer $tenderOffer)
    {
        $tenderOffer->st = 'Revisi';
        $tenderOffer->update();
        Notification::send($tenderOffer->partner, new OfferRevisi($tenderOffer->tender,$tenderOffer));
        return response()->json([
            'alert' => 'success',
            'message' => 'Penawaran direvisi',
        ]);
    }
    public function acc(TenderOffer $tenderOffer)
    {
        $tender = Tender::where('id','=',$tenderOffer->tender_id)->first();
        $tender->nilai_penyerapan = $tenderOffer->nilai;
        $tender->st = "Berjalan";
        $tender->start_at = date("Y-m-d");
        $tender->winner_at = date("Y-m-d");
        $tender->update();
        $tenderOffer->st = 'Penawaran diterima';
        $tenderOffer->update();
        Notification::send($tenderOffer->partner, new OfferAccepted($tenderOffer->tender,$tenderOffer));
        return response()->json([
            'alert' => 'success',
            'message' => 'Penawaran diterima',
        ]);
    }
}
