<?php

namespace App\Http\Controllers\Office\Pengadaan;

use App\Models\Kbli;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PartnerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $kbli = Kbli::get();
            $keywords = $request->keyword;
            $kategori = $request->kategori;
            if($kategori == "tolak"){
                $collection = Partner::where('nama','LIKE','%'.$keywords.'%')->where('reason','!=',null)->orderBy('created_at', 'desc')->paginate(10);
            }elseif($kategori == "belum"){
                $collection = Partner::where('nama','LIKE','%'.$keywords.'%')->where('verified_at','=',null)->where('reason','=',null)->orderBy('created_at', 'desc')->paginate(10);
            }elseif($kategori == "sudah"){
                $collection = Partner::where('nama','LIKE','%'.$keywords.'%')->where('verified_at','!=',null)->orderBy('created_at', 'desc')->paginate(10);
            }else{
                $collection = Partner::where('nama','LIKE','%'.$keywords.'%')->orderBy('created_at', 'desc')->paginate(10);
            }
            return view('page.office.partner.list', compact('collection','kbli'));
        }
        return view('page.office.partner.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Partner $partner)
    {
        $kbli = Kbli::get();
        $dokumen = $partner->dokumen;
        return view('page.office.partner.show',compact('partner','dokumen','kbli'));
    }
    public function show_doc(Partner $partner)
    {
        $dokumen = $partner->dokumen;
        return view('page.office.partner.show_doc',compact('partner','dokumen'));
    }
    public function edit(Partner $partner)
    {
        //
    }
    public function update(Request $request, Partner $partner)
    {
        //
    }
    public function destroy(Partner $partner)
    {
        //
    }
    public function verif(Partner $partner){
        $partner->verified_at = date("Y-m-d H:i:s");
        $partner->reason = null;
        $partner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Rekanan terverifikasi',
        ]);
    }
    public function reason(Partner $partner){
        return view('page.office.partner.reason',['data' => $partner]);
    }
    public function store_reason(Partner $partner, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'alasan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('alasan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alasan'),
                ]);
            }
        }
        $partner->reason = $request->alasan;
        $partner->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Alasan tersimpan',
        ]);
    }
}