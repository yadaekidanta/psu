<?php

namespace App\Http\Controllers\Office\Pengadaan;

use App\Models\PartnerDoc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PartnerDocController extends Controller
{
    public function download(PartnerDoc $partnerDoc)
    {
        return Storage::download($partnerDoc->file);
    }
}