<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    protected function redirectTo($request)
    {
        if (request()->getHttpHost() === 'ptpsumut.com') {
            if (! $request->expectsJson()) {
                return route('web.auth.index');
            }
        }
        if (request()->getHttpHost() === 'office.ptpsumut.com') {
            if (! $request->expectsJson()) {
                return route('office.auth.index');
            }
        }
        if (request()->getHttpHost() === 'eproc.ptpsumut.com') {
            if (! $request->expectsJson()) {
                return route('eproc.auth.index');
            }
        }
    }
}
