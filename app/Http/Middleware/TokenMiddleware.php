<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TokenMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $url        = 'https://djponline.pajak.go.id/account/login';

        $ch         = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $html       = curl_exec($ch);

        //extract html to get the data
        libxml_use_internal_errors(true);
        $dom        = new \DOMDocument();

        $scripts    = $dom->getElementsByTagName('script');

        //get token for future request
        $script     = $scripts[0];
        $nodeValue  = $script->nodeValue;
        preg_match('/\window\.([a-zA-Z0-9][a-zA-Z0-9])\s*={(.*?)};/', $nodeValue, $matches);
        preg_match('/"([^"]+)"/', $matches[0], $token);
        $token      = $token[1];
        $request->merge(compact('token'));
        libxml_clear_errors();

        return $next($request);
    }
}
