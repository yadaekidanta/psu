<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Web\MediaController;
use App\Http\Controllers\Web\RouteController;
use App\Http\Controllers\Web\LaporanController;
use App\Http\Controllers\Web\ProductController;

Route::group(['domain' => 'https://ptpsumut.com'], function() {
    Route::get('/',[RouteController::class, 'index'])->name('web.home');
    Route::get('highlight',[RouteController::class, 'highlight'])->name('web.highlight');
    Route::get('komisaris-direksi',[RouteController::class, 'komisaris_direksi'])->name('web.komisaris_direksi');
    Route::get('struktur-organisasi',[RouteController::class, 'struktur_organisasi'])->name('web.struktur_organisasi');
    Route::get('visi-misi',[RouteController::class, 'visi_misi'])->name('web.visi_misi');
    Route::get('mitra-pelanggan',[RouteController::class, 'mitra_pelanggan'])->name('web.mitra_pelanggan');
    Route::get('sertifikasi-penghargaan',[RouteController::class, 'sertifikasi_penghargaan'])->name('web.sertifikasi_penghargaan');
    Route::get('product',[ProductController::class, 'index'])->name('web.product.index');
    Route::get('product/{product:slug}',[ProductController::class, 'show'])->name('web.product.show');
    Route::get('berita',[MediaController::class, 'index_berita'])->name('web.berita.index');
    Route::get('berita/{media:slug}',[MediaController::class, 'berita_show'])->name('web.berita.show');
    Route::get('galeri',[MediaController::class, 'index_galeri'])->name('web.galeri.index');
    Route::get('galeri/{media:slug}',[MediaController::class, 'galeri_show'])->name('web.galeri.show');
    Route::get('pengumuman',[MediaController::class, 'index_pengumuman'])->name('web.pengumuman.index');
    Route::get('pengumuman/{media:slug}',[MediaController::class, 'pengumuman_show'])->name('web.pengumuman.show');
    Route::get('laporan',[LaporanController::class, 'index'])->name('web.laporan');
    Route::get('laporan/{laporan:slug}',[LaporanController::class, 'show'])->name('web.laporan.show');
    Route::get('contact',[RouteController::class, 'kontak'])->name('web.contact');
    Route::post('contact/store',[RouteController::class, 'kirim_pesan'])->name('web.contact.store');
    Route::get('karir',[RouteController::class, 'karir'])->name('web.karir');
    Route::get('migrate', function(){
        Artisan::call('migrate');
        return response()->json([
            'alert' => 'success',
            'message' => 'DB Migrate!'
        ]);
    })->name('db.migrate');
    Route::get('storage-link', function(){
        Artisan::call('storage:link');
        return response()->json([
            'alert' => 'success',
            'message' => 'Storage Linked!'
        ]);
    })->name('storage.link');
    Route::get('db-seed', function(){
        Artisan::call('db:seed');
        return response()->json([
            'alert' => 'success',
            'message' => 'DB Seed!'
        ]);
    })->name('db.seed');
});