<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Office\AuthController;
use App\Http\Controllers\Office\PesanController;
use App\Http\Controllers\Office\LaporanController;
use App\Http\Controllers\Office\ProfileController;
use App\Http\Controllers\Office\Sdm\JobController;
use App\Http\Controllers\Office\RegionalController;
use App\Http\Controllers\Office\DashboardController;
use App\Http\Controllers\Office\LaporanTypeController;
use App\Http\Controllers\Office\Master\BankController;
use App\Http\Controllers\Office\Master\KbliController;
use App\Http\Controllers\Office\Master\RoleController;
use App\Http\Controllers\Office\NotificationController;
use App\Http\Controllers\Office\Sdm\EmployeeController;
use App\Http\Controllers\Office\Master\DivisiController;
use App\Http\Controllers\Office\Master\DocTypeController;
use App\Http\Controllers\Office\Master\ProductController;
use App\Http\Controllers\Office\Master\LocationController;
use App\Http\Controllers\Office\Master\JenisKbliController;
use App\Http\Controllers\Office\Pengadaan\TenderController;
use App\Http\Controllers\Office\Pengadaan\PartnerController;
use App\Http\Controllers\Office\Master\PartnerTypeController;
use App\Http\Controllers\Office\Pengadaan\PartnerDocController;
use App\Http\Controllers\Office\Pengadaan\TenderOfferController;
use App\Http\Controllers\Office\Pengaturan\Web\BeritaController;
use App\Http\Controllers\Office\Pengaturan\Web\GaleriController;
use App\Http\Controllers\Office\Pengaturan\Web\KontenController;
use App\Http\Controllers\Office\Pengaturan\Web\PengumumanController;
use App\Http\Controllers\Office\Pengaturan\Web\MediaGaleriController;

Route::group(['domain' => 'https://office.ptpsumut.com'], function() {
    Route::name('office.')->group(function(){
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::get('',[AuthController::class, 'index'])->name('index');
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
        });

        Route::middleware(['auth:office'])->group(function(){
            Route::redirect('/', 'dashboard', 301);
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::get('counter_notif', [NotificationController::class, 'counter'])->name('counter_notif');
            Route::get('notification', [NotificationController::class, 'index'])->name('notification');
            // master
            Route::resource('pesan', PesanController::class);
            Route::resource('bank', BankController::class);
            Route::resource('partner-type', PartnerTypeController::class);
            Route::resource('jenis-kbli', JenisKbliController::class);
            Route::get('jenis-kbli/{jenisKbli}/kbli',[JenisKbliController::class, 'index_kbli'])->name('kbli.index');
            Route::get('jenis-kbli/{jenisKbli}/kbli/create',[JenisKbliController::class, 'create_kbli'])->name('kbli.create');
            Route::post('kbli/store',[KbliController::class, 'store'])->name('kbli.store');
            Route::get('kbli/{kbli}/edit',[KbliController::class, 'edit'])->name('kbli.edit');
            Route::patch('kbli/{kbli}/update',[KbliController::class, 'update'])->name('kbli.update');
            Route::delete('kbli/{kbli}/delete',[KbliController::class, 'destroy'])->name('kbli.destroy');
            Route::resource('doc-type', DocTypeController::class);
            Route::resource('product', ProductController::class);
            Route::post('product/{product}/active',[ProductController::class, 'active'])->name('product.active');
            Route::post('product/{product}/non_active',[ProductController::class, 'non_active'])->name('product.non_active');
            Route::resource('location', LocationController::class);
            Route::resource('divisi', DivisiController::class);
            Route::post('divisi/get_list',[DivisiController::class, 'list_option'])->name('divisi.list_option');
            Route::resource('role', RoleController::class);
            Route::post('role/get_list',[RoleController::class, 'list_option'])->name('role.list_option');
            // sdm
            Route::resource('employee', EmployeeController::class);
            Route::resource('job', JobController::class);
            // pengadaan
            Route::resource('partner', PartnerController::class);
            Route::post('partner/{partner}/show_detil',[PartnerController::class, 'show'])->name('partner.show_detil');
            Route::post('partner/{partner}/show_kbli',[PartnerController::class, 'show_kbli'])->name('partner.show_kbli');
            Route::post('partner/{partner}/show_docs',[PartnerController::class, 'show_doc'])->name('partner.show_doc');
            Route::get('partner/{partner}/reason',[PartnerController::class, 'reason'])->name('partner.reason');
            Route::patch('partner/{partner}/reason-store',[PartnerController::class, 'store_reason'])->name('partner.reason.store');
            Route::post('partner/{partner}/verif',[PartnerController::class, 'verif'])->name('partner.verif');
            Route::get('doc/{partnerDoc}/download',[PartnerDocController::class, 'download'])->name('doc.download');
            Route::resource('tender', TenderController::class);
            Route::patch('tender/{tender}/update_spk',[TenderController::class, 'update_spk'])->name('tender.update_spk');
            Route::post('tender/{tender}/publish',[TenderController::class, 'publish'])->name('tender.publish');
            Route::post('tender/{tender}/show_penawaran',[TenderController::class, 'show_penawaran'])->name('tender.show_penawaran');
            Route::post('tender/{tender}/selesai',[TenderController::class, 'selesai'])->name('tender.selesai');
            Route::get('offer/{tenderOffer}/download',[TenderOfferController::class, 'download'])->name('offer.download');
            Route::post('offer/{tenderOffer}/revisi',[TenderOfferController::class, 'revisi'])->name('offer.revisi');
            Route::post('offer/{tenderOffer}/acc',[TenderOfferController::class, 'acc'])->name('offer.acc');
            // laporan
            Route::resource('laporan', LaporanController::class);
            Route::get('laporan/{laporan}/open',[LaporanController::class, 'open_file'])->name('laporan.open_file');
            Route::get('laporan/{laporan}/download',[LaporanController::class, 'download_file'])->name('laporan.download_file');
            Route::resource('laporan-type', LaporanTypeController::class);
            // pengaturan web
            Route::resource('konten', KontenController::class);
            Route::resource('berita', BeritaController::class);
            Route::resource('galeri', GaleriController::class);
            Route::resource('pengumuman', PengumumanController::class);
            Route::get('media/{media}/create',[MediaGaleriController::class, 'create'])->name('media.create');
            Route::post('media/store',[MediaGaleriController::class, 'store'])->name('media.store');
            Route::get('media/{media}/{mediaGaleri}/edit',[MediaGaleriController::class, 'edit'])->name('media.edit');
            Route::patch('media/{mediaGaleri}/update',[MediaGaleriController::class, 'update'])->name('media.update');
            Route::delete('media/{mediaGaleri}/destroy',[MediaGaleriController::class, 'destroy'])->name('media.destroy');
            // pengaturan eproc
            Route::get('konten-eproc', [KontenController::class, 'index_eproc'])->name('konten.eproc');
            Route::get('konten-eproc/{konten}/edit', [KontenController::class, 'edit_eproc'])->name('konten.eproc.edit');
            Route::patch('konten-eproc/{konten}/update', [KontenController::class, 'update_eproc'])->name('konten.eproc.update');
            
            // REGIONAL
            Route::get('province', [RegionalController::class, 'province'])->name('regional.province');
            Route::get('city', [RegionalController::class, 'city'])->name('regional.city');
            Route::get('subdistrict', [RegionalController::class, 'subdistrict'])->name('regional.subdistrict');
            Route::get('village', [RegionalController::class, 'village'])->name('regional.village');
            Route::post('city/get', [RegionalController::class, 'get_city'])->name('regional.get_city');
            Route::post('subdistrict/get', [RegionalController::class, 'get_subdistrict'])->name('regional.get_subdistrict');
            Route::post('village/get', [RegionalController::class, 'get_village'])->name('regional.get_village');
            
            // PROFILE
            Route::prefix('profile')->name('profile.')->group(function(){
                Route::get('', [ProfileController::class, 'index'])->name('index');
                Route::get('edit', [ProfileController::class, 'edit'])->name('edit');
                Route::post('edit', [ProfileController::class, 'change'])->name('change');
                Route::get('edit_password', [ProfileController::class, 'edit_password'])->name('edit_password');
                Route::post('edit_password', [ProfileController::class, 'change_password'])->name('change_password');
                Route::post('save', [ProfileController::class, 'save'])->name('save');
            });

            Route::get('notice',[AuthController::class, 'verification'])->name('verification.notice');
            Route::get('resend',[AuthController::class, 'resend_mail'])->name('auth.resend.mail');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
        });
    });
});