<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Eproc\AuthController;
use App\Http\Controllers\Eproc\TenderController;
use App\Http\Controllers\Eproc\ProfileController;
use App\Http\Controllers\Eproc\RegionalController;
use App\Http\Controllers\Eproc\DashboardController;
use App\Http\Controllers\Eproc\PartnerDocController;
use App\Http\Controllers\Eproc\TenderOfferController;
use App\Http\Controllers\Eproc\NotificationController;

Route::group(['domain' => 'https://eproc.ptpsumut.com'], function() {
    Route::get('',[DashboardController::class, 'guest'])->name('eproc.home');
    Route::post('tentang',[DashboardController::class, 'about'])->name('eproc.tentang');
    Route::post('panduan',[DashboardController::class, 'guide'])->name('eproc.panduan');
    Route::post('contact',[DashboardController::class, 'contact'])->name('eproc.contact');
    Route::post('contact/store',[DashboardController::class, 'kirim_pesan'])->name('eproc.contact.store');
    Route::name('eproc.')->group(function(){
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::get('',[AuthController::class, 'index'])->name('index');
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('check-npwp',[AuthController::class, 'check_npwp'])->middleware('npwp.token')->name('check_npwp');
            // Route::post('check-npwp',[AuthController::class, 'check_npwp'])->middleware('npwp.token')->name('check_npwp');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::get('verify/{auth:email}',[AuthController::class, 'do_verify'])->name('verify');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('getreset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('reset');
        });
        Route::middleware(['auth:procurement'])->group(function(){
            // Route::redirect('/', 'dashboard', 301);
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::get('counter_notif', [NotificationController::class, 'counter'])->name('counter_notif');
            Route::get('notification', [NotificationController::class, 'index'])->name('notification');
            Route::get('tender', [TenderController::class, 'index'])->name('tender.index');
            Route::post('doc/store', [PartnerDocController::class, 'store'])->name('doc.store');
            Route::get('doc/{partnerDoc}/download', [PartnerDocController::class, 'download'])->name('doc.download');
            Route::delete('doc/{partnerDoc}/delete', [PartnerDocController::class, 'destroy'])->name('doc.destroy');
            Route::get('tender/{tender:slug}', [TenderController::class, 'show'])->name('tender.show');
            Route::get('tender/{tender:slug}/download', [TenderController::class, 'download'])->name('tender.download');
            Route::get('tender/{tender:slug}/download_spk', [TenderController::class, 'download_spk'])->name('tender.download_spk');
            Route::get('tender/{tender:slug}/send-offer', [TenderController::class, 'create_penawaran'])->name('tender.create_penawaran');
            Route::get('my-tender', [TenderOfferController::class, 'index'])->name('my-tender.index');
            Route::get('my-tender/{tenderOffer}', [TenderOfferController::class, 'show'])->name('my-tender.show');
            Route::post('my-tender/store', [TenderOfferController::class, 'store'])->name('my-tender.store');
            
            Route::prefix('profile')->name('profile.')->group(function(){
                Route::get('', [ProfileController::class, 'index'])->name('index');
                Route::get('change-profile', [ProfileController::class, 'index_profile'])->name('index_profile');
                Route::get('change-domisili', [ProfileController::class, 'index_domisili'])->name('index_domisili');
                Route::get('change-bank', [ProfileController::class, 'index_bank'])->name('index_bank');
                Route::get('change-document', [ProfileController::class, 'index_document'])->name('index_document');
                Route::get('change-password', [ProfileController::class, 'index_password'])->name('index_password');
                Route::post('change-profile', [ProfileController::class, 'change_profile'])->name('change_profile');
                Route::post('change-domisili', [ProfileController::class, 'change_domisili'])->name('change_domisili');
                Route::post('change-bank', [ProfileController::class, 'change_bank'])->name('change_bank');
                Route::post('change-document', [ProfileController::class, 'change_document'])->name('change_document');
                Route::post('change-password', [ProfileController::class, 'change_password'])->name('change_password');
                Route::get('download-ktp', [ProfileController::class, 'download_ktp'])->name('download_ktp');
                Route::get('download-npwp', [ProfileController::class, 'download_npwp'])->name('download_npwp');
                Route::get('download-compro', [ProfileController::class, 'download_compro'])->name('download_compro');
            });
            Route::post('regional/list_city', [RegionalController::class, 'list_city'])->name('regional.list_city');
            Route::post('regional/list_subdistrict', [RegionalController::class, 'list_subdistrict'])->name('regional.list_subdistrict');

            Route::get('notice',[AuthController::class, 'verification'])->name('verification.notice');
            Route::get('resend',[AuthController::class, 'resend_mail'])->name('auth.resend.mail');
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
        });
    });
});