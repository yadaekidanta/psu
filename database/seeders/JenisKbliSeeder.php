<?php

namespace Database\Seeders;

use App\Models\JenisKbli;
use Illuminate\Database\Seeder;

class JenisKbliSeeder extends Seeder
{
    public function run()
    {
        $jenis = array(
            [
                'nama' => 'Pertanian, Kehutanan, dan Perikanan'
            ],
            [
                'nama' => 'Pertambangan dan Penggalian'
            ],
            [
                'nama' => 'Industri Pengolahan'
            ],
            [
                'nama' => 'Pengadaan Listrik, Gas, Uap/Air Panas Dan Udara Dingin'
            ],
            [
                'nama' => 'Treatment Air & Air Limbah, Treatment & Pemulihan Material Sampah, Aktivitas Remediasi'
            ],
            [
                'nama' => 'Konstruksi'
            ],
            [
                'nama' => 'Perdagangan Besar Dan Eceran; Reparasi Dan Perawatan Mobil Dan Sepeda Motor'
            ],
            [
                'nama' => 'Pengangkutan dan Pergudangan'
            ],
            [
                'nama' => 'Penyediaan Akomodasi Dan Penyediaan Makan Minum'
            ],
            [
                'nama' => 'Informasi Dan Komunikasi'
            ],
            [
                'nama' => 'Aktivitas Keuangan dan Asuransi'
            ],
            [
                'nama' => 'Real Estat'
            ],
            [
                'nama' => 'Aktivitas Profesional, Ilmiah Dan Teknis'
            ],
            [
                'nama' => 'Aktivitas Penyewaan dan Sewa Guna Usaha Tanpa Hak Opsi, Ketenagakerjaan, Agen Perjalanan'
            ],
            [
                'nama' => 'Administrasi Pemerintahan, Pertahanan Dan Jaminan Sosial Wajib'
            ],
            [
                'nama' => 'Pendidikan'
            ],
            [
                'nama' => 'Aktivitas Kesehatan Manusia Dan Aktivitas Sosial'
            ],
            [
                'nama' => 'Kesenian, Hiburan Dan Rekreasi'
            ],
            [
                'nama' => 'Aktivitas Jasa Lainnya'
            ],
            [
                'nama' => 'Aktivitas Rumah Tangga Sbg Pemberi Kerja; Aktivitas Yg Menghasilkan Barang Jasa Oleh Rumah Tangga'
            ],
            [
                'nama' => 'Aktivitas Badan Internasional Dan Badan Ekstra Internasional Lainnya'
            ],
        );
        foreach($jenis AS $j){
            JenisKbli::create([
                'nama' => $j['nama']
            ]);
        }
    }
}
