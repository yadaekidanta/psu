<?php

namespace Database\Seeders;

use App\Models\DocType;
use Illuminate\Database\Seeder;

class FileTypeSeeder extends Seeder
{
    public function run()
    {
        $tipe = array(
            [
                'nama' => 'No Akta Pendirian'
            ],
            [
                'nama' => 'NIB'
            ],
            [
                'nama' => 'TDP'
            ],
            [
                'nama' => 'SIUI'
            ],
            [
                'nama' => 'SIUP'
            ],
            [
                'nama' => 'SIUJK'
            ],
            [
                'nama' => 'Company Profile'
            ],
            [
                'nama' => 'Lainnya'
            ],
        );
        foreach($tipe AS $t){
            DocType::create([
                'nama' => $t['nama']
            ]);
        }
    }
}
