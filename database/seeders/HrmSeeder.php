<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Divisi;
use App\Models\Employee;
use App\Models\Location;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class HrmSeeder extends Seeder
{
    public function run()
    {
        $lokasi = array(
            [
                'nama' => 'Kantor Direksi'
            ],
            [
                'nama' => 'Kebun'
            ]
        );
        $divisi = array(
            [
                'nama' => 'Super Admin'
            ],
            [
                'nama' => 'Komisaris'
            ],
            [
                'nama' => 'Direksi'
            ],
            [
                'nama' => 'Corporate Secretary'
            ],
            [
                'nama' => 'SPI'
            ],
            [
                'nama' => 'Umum'
            ],
            [
                'nama' => 'Komersil'
            ],
            [
                'nama' => 'Keuangan'
            ],
            [
                'nama' => 'Produksi'
            ],
            [
                'nama' => 'Teknik'
            ],
            [
                'nama' => 'Pengembangan'
            ],
        );
        $role = array(
            [
                'divisi_id' => '1',
                'nama' => 'Administrator'
            ],
            [
                'divisi_id' => '2',
                'nama' => 'Dewan Komisaris'
            ],
            [
                'divisi_id' => '2',
                'nama' => 'Sekretaris Komisaris'
            ],
            [
                'divisi_id' => '2',
                'nama' => 'Komite Audit'
            ],
            [
                'divisi_id' => '3',
                'nama' => 'Direktur Utama'
            ],
            [
                'divisi_id' => '3',
                'nama' => 'Direktur Keuangan & Umum'
            ],
            [
                'divisi_id' => '3',
                'nama' => 'Direktur Produksi & Pengembangan'
            ],
            [
                'divisi_id' => '4',
                'nama' => 'Kepala Sekretaris Perusahaan'
            ],
            [
                'divisi_id' => '4',
                'nama' => 'Wakil Kepala Sekretaris Perusahaan'
            ],
            [
                'divisi_id' => '5',
                'nama' => 'KA SPI'
            ],
            [
                'divisi_id' => '6',
                'nama' => 'KA Umum'
            ],
            [
                'divisi_id' => '7',
                'nama' => 'KA Komersil'
            ],
            [
                'divisi_id' => '8',
                'nama' => 'KA Keuangan'
            ],
            [
                'divisi_id' => '9',
                'nama' => 'KA Produksi'
            ],
            [
                'divisi_id' => '10',
                'nama' => 'KA Teknik'
            ],
            [
                'divisi_id' => '11',
                'nama' => 'KA Pengembangan'
            ],
        );
        $employee = array(
            [
                'nama' => 'Rizky Ramadhan',
                'email' => 'officialrizkyr@gmail.com',
                'hp' => '087709020299',
                'password' => Hash::make('password'),
            ]
        );
        foreach($lokasi AS $l){
            Location::create([
                'nama' => $l['nama']
            ]);
        }
        foreach($divisi AS $d){
            Divisi::create([
                'lokasi_id' => 1,
                'nama' => $d['nama']
            ]);
        }
        foreach($role AS $r){
            Role::create([
                'lokasi_id' => 1,
                'divisi_id' => $r['divisi_id'],
                'nama' => $r['nama']
            ]);
        }
        foreach($employee AS $e){
            Employee::create([
                'nip' => '123456789',
                'nama' => $e['nama'],
                'email' => $e['email'],
                'hp' => $e['hp'],
                'tanggal_lahir' => '1996-02-15',
                'tempat_lahir' => 'Jakarta',
                'tanggal_pengangkatan' => '1996-02-15',
                'jk' => 'Laki-Laki',
                'agama' => 'Islam',
                'alamat' => 'Komplek Permata Buah Batu Blok C 15B',
                'alamat_domisili' => 'Komplek Permata Buah Batu Blok C 15B',
                'lokasi_id' => 1,
                'divisi_id' => 1,
                'role_id' => 1,
                'password' => $e['password'],
            ]);
        }
    }
}