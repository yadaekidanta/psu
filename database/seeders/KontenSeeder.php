<?php

namespace Database\Seeders;

use App\Models\Konten;
use Illuminate\Database\Seeder;

class KontenSeeder extends Seeder
{
    public function run()
    {
        $konten = array(
            [
                'isi' => 'Highlight',
                'tipe' => 'highlight'
            ],
            [
                'isi' => 'struktur organisasi',
                'tipe' => 'struktur-organisasi'
            ],
            [
                'isi' => 'visi dan misi',
                'tipe' => 'visi-misi'
            ],
            [
                'isi' => 'Mitra dan Pelanggan',
                'tipe' => 'mitra-pelanggan'
            ],
            [
                'isi' => 'Sertifikasi dan Penghargaan',
                'tipe' => 'sertifikasi-penghargaan'
            ],
            [
                'isi' => 'Tentang Eproc',
                'tipe' => 'tentang-eproc'
            ],
            [
                'isi' => 'Panduan Eproc',
                'tipe' => 'panduan-eproc'
            ],
        );
        foreach($konten AS $k){
            Konten::create([
                'isi' => $k['isi'],
                'tipe' => $k['tipe']
            ]);
        }
    }
}
