<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            RegionalSeeder::class,
            JenisKbliSeeder::class,
            FileTypeSeeder::class,
            HrmSeeder::class,
            KontenSeeder::class,
        ]);
    }
}
