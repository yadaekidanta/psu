<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterModule extends Migration
{
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->morphs('notifiable');
            $table->text('data');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
        });
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });
        // Schema::create('banner', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('gambar');
        //     $table->longText('url')->nullable();
        //     $table->enum('st',['a','n'])->default('a');
        //     $table->timestamps();
        // });
        Schema::create('pesan', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->string('hp',15)->nullable();
            $table->string('subject')->nullable();
            $table->longText('pesan')->nullable();
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
        });
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->string('slug')->nullable();
            $table->longText('deskripsi')->nullable();
            $table->string('gambar')->nullable();
            $table->string('harga_dasar')->nullable();
            $table->string('harga_jual')->nullable();
            $table->string('stok')->nullable();
            $table->enum('publish',['y','n'])->default('y');
            $table->timestamps();
        });
        Schema::create('konten', function (Blueprint $table) {
            $table->id();
            $table->longText('isi');
            $table->string('tipe');
            // $table->enum('tipe',['highlight','struktur_organisasi','visi','misi']);
        });
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('slug');
            $table->longText('isi');
            $table->string('thumbnail')->nullable();
            $table->enum('tipe',['berita','galeri','pengumuman']);
            $table->timestamps();
        });
        Schema::create('media_galeri', function (Blueprint $table) {
            $table->id();
            $table->string('media_id');
            $table->string('gambar');
        });
        Schema::create('laporan_type', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
        });
        Schema::create('laporan', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('slug');
            $table->string('file');
            $table->string('tahun',4);
            $table->integer('laporan_type_id')->default(0);
            $table->timestamps();
        });
        Schema::create('tender', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('slug');
            $table->longText('deskripsi')->nullable();
            // $table->longText('requirement')->nullable();
            $table->string('attachment')->nullable();
            $table->string('no_po',50)->nullable();
            $table->string('no_spk',50)->nullable();
            $table->string('attachment_po')->nullable();
            $table->string('attachment_spk')->nullable();
            $table->string('nilai_tender',30)->default(0);
            $table->string('nilai_penyerapan',30)->default(0);
            $table->string('st')->nullable();
            $table->date('tender_deadline_at')->nullable();
            $table->date('work_deadline_at')->nullable();
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->date('winner_at')->nullable();
            $table->date('contract_at')->nullable();
            $table->timestamps();
        });
        Schema::create('tender_offer', function (Blueprint $table) {
            $table->id();
            $table->integer('tender_id')->default(0);
            $table->integer('partner_id')->default(0);
            $table->string('file')->nullable();
            $table->longText('pesan')->nullable();
            $table->string('nilai',30)->default(0);
            $table->string('st')->nullable();
            $table->timestamps();
        });
        Schema::create('tender_invoice', function (Blueprint $table) {
            $table->id();
            $table->integer('tender_id')->default(0);
            $table->integer('partner_id')->default(0);
            $table->string('code',50)->nullable();
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
        // Schema::create('menu', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('judul')->nullable();
        //     $table->integer('icon_id')->default(0);
        //     $table->integer('menu_id')->default(0);
        //     $table->string('urutan',3)->default(0);
        //     $table->longText('route')->nullable();
        // });
    }
    public function down()
    {
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('failed_jobs');
        // Schema::dropIfExists('banner');
        Schema::dropIfExists('pesan');
        Schema::dropIfExists('product');
        Schema::dropIfExists('konten');
        Schema::dropIfExists('media');
        Schema::dropIfExists('media_galeri');
        Schema::dropIfExists('laporan_type');
        Schema::dropIfExists('laporan');
        Schema::dropIfExists('tender');
        Schema::dropIfExists('tender_offer');
        Schema::dropIfExists('tender_invoice');
        // Schema::dropIfExists('menu');
    }
}
