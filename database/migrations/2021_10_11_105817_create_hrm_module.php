<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrmModule extends Migration
{
    public function up()
    {
        Schema::create('users_verify', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('token');
            $table->timestamps();
        });
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
        Schema::create('lokasi', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
        });
        Schema::create('divisi', function (Blueprint $table) {
            $table->id();
            $table->integer('lokasi_id')->default(0);
            $table->string('nama');
        });
        Schema::create('role', function (Blueprint $table) {
            $table->id();
            $table->integer('lokasi_id')->default(0);
            $table->integer('divisi_id')->default(0);
            $table->string('nama');
        });
        Schema::create('bank', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
        });
        Schema::create('doc_type', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
        });
        Schema::create('jenis_kbli', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
        });
        Schema::create('kbli', function (Blueprint $table) {
            $table->id();
            $table->integer('jenis_kbli_id')->nullable();
            $table->string('kode',5)->nullable();
            $table->string('nama')->nullable();
        });
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('slug');
            $table->longText('deskripsi');
            $table->longText('requirement');
            $table->longText('fasilitas');
            $table->integer('lokasi_id')->default(0);
            $table->integer('divisi_id')->default(0);
            $table->integer('role_id')->default(0);
            $table->string('gaji',20)->default(0);
            $table->timestamps();
        });
        Schema::create('job_applicants', function (Blueprint $table) {
            $table->id();
            $table->integer('jobs_id')->default(0);
            $table->integer('users_id')->default(0);
            $table->string('expected_salary',20)->default(0);
            $table->longText('pesan')->nullable();
            $table->timestamps();
        });
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('email')->nullable();
            $table->string('hp',15)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->enum('jk',['Laki-Laki','Perempuan'])->nullable();
            $table->enum('agama',['Islam','Katolik','Protestan','Hindu','Buddha','Lainnya'])->nullable();
            $table->string('avatar')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
        Schema::create('employee', function (Blueprint $table) {
            $table->id();
            $table->string('nip',30)->nullable();
            $table->string('nama',100);
            $table->string('email')->nullable();
            $table->string('hp',15)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_pengangkatan')->nullable();
            $table->enum('jk',['Laki-Laki','Perempuan'])->nullable();
            $table->enum('agama',['Islam','Katolik','Protestan','Hindu','Buddha','Lainnya'])->nullable();
            $table->longText('alamat')->nullable();
            $table->longText('alamat_domisili')->nullable();
            $table->integer('lokasi_id')->default(0);
            $table->integer('divisi_id')->default(0);
            $table->integer('role_id')->default(0);
            $table->string('avatar')->nullable();
            $table->string('password')->nullable();
            $table->timestamps();
        });
        Schema::create('partner', function (Blueprint $table) {
            $table->id();
            $table->string('nama',100);
            $table->string('nama_pendiri',100)->nullable();
            $table->string('nama_perusahaan');
            $table->string('email')->nullable();
            $table->string('hp',15)->nullable();
            $table->longText('bidang_usaha')->nullable();
            $table->longText('alamat')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('subdistrict_id')->nullable();
            $table->string('postcode',6)->nullable();
            $table->string('avatar')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('no_ktp',16)->nullable();
            $table->string('no_npwp',20)->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('no_rek')->nullable();
            $table->string('rek_an')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->longText('reason')->nullable();
            $table->integer('partner_type_id')->nullable();
            $table->timestamps();
        });
        Schema::create('partner_type', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->enum('simbol',['<','<=','=','>','>='])->nullable();
            $table->string('nilai')->nullable();
        });
        Schema::create('partner_doc', function (Blueprint $table) {
            $table->id();
            $table->integer('partner_id')->nullable();
            $table->integer('doc_type_id')->nullable();
            $table->string('nomor')->nullable();
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('users_verify');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('lokasi');
        Schema::dropIfExists('divisi');
        Schema::dropIfExists('role');
        Schema::dropIfExists('bank');
        Schema::dropIfExists('doc_type');
        Schema::dropIfExists('bidang_usaha');
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('job_applicants');
        Schema::dropIfExists('users');
        Schema::dropIfExists('employee');
        Schema::dropIfExists('partner');
        Schema::dropIfExists('partner_type');
        Schema::dropIfExists('partner_doc');
    }
}