$("body").on("contextmenu", "img", function(e) {
    return false;
});
$('img').attr('draggable', false);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function number_only(obj) {
    $('#' + obj).bind('keypress', function (event) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
}
function npwp_format(obj){
    Inputmask({
        "mask" : "99.999.999.9-999.999"
    }).mask("#" + obj);
}
function auth_content(obj){
    $("#page_login").hide();
    $("#page_register").hide();
    $("#page_forgot").hide();
    $("#" + obj).show();    
    if(obj == "page_login"){
        $("#email").focus();
    }else if(obj == "page_register"){
        $("#nama").focus();
    }else{
        $("#mail").focus();
    }
}
$("#form_login").on('keydown', 'input', function(event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-login'));
        var val = $($this).val();
        if (val < 2) {
            $('[data-login="' + (index + 1).toString() + '"]').focus();
        } else {
            $('#tombol_login').trigger("click");
        }
    }
});
$("#form_register").on('keydown', 'input', function(event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-register'));
        var val = $($this).val();
        if (val < 6) {
            $('[data-register="' + (index + 1).toString() + '"]').focus();
        } else {
            $('#tombol_register').trigger("click");
        }
    }
});
$("#form_forgot").on('keydown', 'input', function(event) {
    if (event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-forgot'));
        var val = $($this).val();
        if (val == 1) {
            $('[data-forgot="' + (index + 1).toString() + '"]').focus();
        } else {
            $('#tombol_forgot').trigger("click");
        }
    }
});
$("#email").focus();
function handle_get(tombol,form,url){
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    $.get(url, $(form).serialize(), function(result) {
        if (result.alert == "success") {
            Swal.fire({ text: result.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } }).then(function() {
                if(result.callback == "reload"){
                    location.reload();
                }else{
                    auth_content(result.callback);
                }
            });
        }else{
            Swal.fire({ text: result.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
        }
        $(tombol).prop("disabled", false);
        $(tombol).removeAttr("data-kt-indicator");
    }, "html");
}
function handle_post(tombol,form,url){
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    $.post(url, $(form).serialize(), function(result) {
        if (result.alert == "success") {
            Swal.fire({ text: result.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } }).then(function() {
                if(result.callback == "reload"){
                    location.reload();
                }else{
                    auth_content(result.callback);
                }
            });
        }else{
            Swal.fire({ text: result.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
        }
        $(tombol).prop("disabled", false);
        $(tombol).removeAttr("data-kt-indicator");
    }, "json");
}