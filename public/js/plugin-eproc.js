function format_npwp(value) {
    if (typeof value === 'string') {
        return value.replace(/(\d{2})(\d{3})(\d{3})(\d{1})(\d{3})(\d{3})/, '$1.$2.$3.$4-$5.$6');
    }
}
function npwp_format(obj){
    $("#" + obj).mask("99.999.999.9-999.999");
}