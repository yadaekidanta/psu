<!DOCTYPE html>
<html lang="en">
	@include('theme.auth.head')
	<body id="kt_body" class="bg-body">
		<div class="d-flex flex-column flex-root">
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url({{asset('keenthemes/media/illustrations/sigma-1/14.png')}}">
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					<a href="../../demo10/dist/index.html" class="mb-12">
						<img alt="Logo" src="{{asset('icon.png')}}" class="h-40px" />
					</a>
					{{$slot}}
				</div>
				<div class="d-flex flex-center flex-column-auto p-10 d-none">
					<div class="d-flex align-items-center fw-bold fs-6">
						<a href="https://keenthemes.com" class="text-muted text-hover-primary px-2">About</a>
						<a href="mailto:support@keenthemes.com" class="text-muted text-hover-primary px-2">Contact</a>
						<a href="https://1.envato.market/EA4JP" class="text-muted text-hover-primary px-2">Contact Us</a>
					</div>
				</div>
			</div>
		</div>
		@include('theme.auth.js')
        @yield('custom_js')
	</body>
</html>