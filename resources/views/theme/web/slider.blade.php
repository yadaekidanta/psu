<section id="slider" class="slider-element slider-parallax min-vh-60 min-vh-md-100 include-header">
    <div class="slider-inner" style="background: #FFF url('{{asset('home.svg')}}') center right no-repeat; background-size: 65%;">

        <div class="vertical-middle slider-element-fade">
            <div class="container py-5">
                <div class="row pt-5">
                    <div class="col-lg-5 col-md-8">
                        <div class="slider-title">
                            <div class="badge rounded-pill badge-default">{{config('app.name')}}</div>
                            {{-- <h2>Improve your SEO with Data.</h2> --}}
                            {{-- <h3 class="text-rotater mb-2" data-separator="," data-rotate="fadeIn" data-speed="3500">- Boost your own <span class="t-rotate">Awesome,Beautiful,Great</span> Website.</h3> --}}
                            <p>
                                {!!Str::limit('<strong>PT Perkebunan Sumatera Utara (Persero)</strong>
                                merupakan salah satu badan usaha milik daerah Provinsi Sumatera Utara,
                                didirikan berdasarkan Peraturan Daerah Tingkat I Sumatera Utara Nomor 16 Tahun 1979
                                dengan bentuk badan hukum yang pertama sekali berupa Perusahaan Daerah (PD).
                                Kemudian disempurnakan dengan Peraturan Daerah Nomor 24 tahun 1985 yang disahkan
                                dengan Keputusan Menteri Dalam Negeri Nomor 539.22-1434 tanggal 16 Oktober 1985
                                dan diundangkan dalam lembaran daerah Provinsi Sumatera Utara tanggal 29 Januari 1986',400)!!}
                            </p>
                            <a href="{{route('web.contact')}}" class="button button-rounded button-large nott ls0">Kontak Kami</a>
                            {{-- <a href="demo-seo-contact.html" class="button button-rounded button-large button-light text-dark bg-white border nott ls0">Contact Us</a> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="video-wrap h-100 d-block d-lg-none">
            <div class="video-overlay" style="background: rgba(255,255,255,0.85);"></div>
        </div>

    </div>
</section>