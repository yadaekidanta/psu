<header id="header" class="transparent-header floating-header header-size-md">
    <div id="header-wrap">
        <div class="container">
            <div class="header-row">
                <!-- Logo ============================================= -->
                <div id="logo">
                    <a href="javascript:;" class="standard-logo" data-dark-logo="{{asset('icon.png')}}"><img src="{{asset('icon.png')}}" alt="{{config('app.name')}}"></a>
                    <a href="javascript:;" class="retina-logo" data-dark-logo="{{asset('icon.png')}}"><img src="{{asset('icon.png')}}" alt="{{config('app.name')}}"></a>
                </div>
                <!-- #logo end -->
                <div class="header-misc">
                    <!-- Top Search ============================================= -->
                    @if(request()->is('berita') || request()->is('galeri') || request()->is('pengumuman'))
                    <div id="top-search" class="header-misc-icon">
                        <a href="javascript:;" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
                    </div>
                    @endif
                    <!-- #top-search end -->
                    {{-- <a href="demo-seo-about.html" class="button button-rounded ms-3 d-none d-sm-block">Get Started</a> --}}
                </div>
                <div id="primary-menu-trigger">
                    <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                </div>
                <!-- Primary Navigation ============================================= -->
                <nav class="primary-menu with-arrows">
                    <ul class="menu-container">
                        <li class="menu-item {{request()->is('/') ? 'current' : ''}}"><a class="menu-link" href="{{route('web.home')}}"><div>Home</div></a></li>
                        <li class="menu-item {{request()->is('highlight') || request()->is('komisaris-direksi') || request()->is('struktur-organisasi') || request()->is('visi-misi') || request()->is('mitra-pelanggan') || request()->is('sertifikasi-penghargaan') ? 'current' : ''}}"><div class="menu-link"><div>Tentang</div></div>
                            <ul class="sub-menu-container">
                                <li class="menu-item {{request()->is('highlight') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.highlight')}}"><div>Highlight</div></a>
                                </li>
                                <li class="menu-item {{request()->is('komisaris-direksi') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.komisaris_direksi')}}"><div>Komisaris &amp; Direksi</div></a>
                                </li>
                                <li class="menu-item {{request()->is('struktur-organisasi') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.struktur_organisasi')}}"><div>Struktur Organisasi</div></a>
                                </li>
                                <li class="menu-item {{request()->is('visi-misi') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.visi_misi')}}"><div>Visi &amp; Misi</div></a>
                                </li>
                                <li class="menu-item {{request()->is('mitra-pelanggan') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.mitra_pelanggan')}}"><div>Mitra &amp; Pelanggan</div></a>
                                </li>
                                <li class="menu-item {{request()->is('sertifikasi-penghargaan') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.sertifikasi_penghargaan')}}"><div>Sertifikasi &amp; Penghargaan</div></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item {{request()->is('product') || request()->is('product/*') ? 'current' : ''}}"><a class="menu-link" href="{{route('web.product.index')}}"><div>Katalog Produk</div></a></li>
                        <li class="menu-item {{request()->is('berita') || request()->is('galeri') || request()->is('pengumuman') || request()->is('berita/*') || request()->is('galeri/*') || request()->is('pengumuman/*') ? 'current' : ''}}"><div class="menu-link"><div>Media</div></div>
                            <ul class="sub-menu-container">
                                <li class="menu-item {{request()->is('berita') || request()->is('berita/*') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.berita.index')}}"><div>Berita</div></a>
                                </li>
                                <li class="menu-item {{request()->is('galeri') || request()->is('galeri/*') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.galeri.index')}}"><div>Galeri</div></a>
                                </li>
                                <li class="menu-item {{request()->is('pengumuman') || request()->is('pengumuman/*') ? 'current' : ''}}">
                                    <a class="menu-link" href="{{route('web.pengumuman.index')}}"><div>Pengumuman</div></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item {{request()->is('laporan') ? 'current' : ''}}"><a class="menu-link" href="{{route('web.laporan')}}"><div>Laporan Keberlanjutan</div></a></li>
                        <li class="menu-item {{request()->is('contact') ? 'current' : ''}}"><a class="menu-link" href="{{route('web.contact')}}"><div>Kontak Kami</div></a></li>
                    </ul>
                </nav>
                <!-- #primary-menu end -->
                <form class="top-search-form" id="content_filter">
                    <input type="text" name="keyword" onkeyup="load_list(1);" class="form-control" placeholder="Cari disini..." autocomplete="off">
                </form>
            </div>
        </div>
    </div>
    <div class="header-wrap-clone"></div>
</header>