<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('theme.web.head')
<body class="stretched">
	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Top Bar ============================================= -->
		@include('theme.web.topbar')
        <!-- #top-bar end -->
		<!-- Header ============================================= -->
		@include('theme.web.header')
        <!-- #header end -->

		<!-- Slider ============================================= -->
		@if (request()->is('/'))
		@include('theme.web.slider')
		@endif
        <!-- #slider end -->
		<!-- Content ============================================= -->
		{{$slot}}
        <!-- #content end -->
		<!-- Footer ============================================= -->
        @include('theme.web.footer')		
        <!-- #footer end -->
	</div>
    <!-- #wrapper end -->
	<!-- Go To Top ============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
	<!-- JavaScripts ============================================= -->
	@include('theme.web.js')
	@yield('custom_js')
</body>
</html>