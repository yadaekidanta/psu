<div id="top-bar" class="transparent-topbar">
    <div class="container clearfix">
        <div class="row justify-content-between">
            <div class="col-12 col-md-auto">
                <!-- Top Links ============================================= -->
                <div class="top-links">
                    <ul class="top-links-container">
                        <li class="top-links-item"><a target="_blank" href="https://eproc.ptpsumut.com/">e-Proc</a></li>
                        <li class="top-links-item"><a href="{{route('web.karir')}}">Karir</a></li>
                        <li class="top-links-item"><a href="{{route('web.contact')}}">Kontak Kami</a></li>
                        <li class="top-links-item">
                            <a href="javascript:;">
                                <img src="{{asset('keenthemes/media/flags/indonesia.svg')}}" alt="Lang">Bahasa Indonesia
                            </a>
                            {{-- <ul class="top-links-sub-menu">
                                <li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/fre.png" alt="Lang">French</a></li>
                                <li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/ara.png" alt="Lang">Arabic</a></li>
                                <li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/tha.png" alt="Lang">Thai</a></li>
                            </ul> --}}
                        </li>
                    </ul>
                </div>
                <!-- .top-links end -->
            </div>
            <div class="col-12 col-md-auto dark">
                <!-- Top Social ============================================= -->
                <ul id="top-social">
                    {{-- <li><a href="https://facebook.com/semicolonweb" class="si-facebook" target="_blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                    <li><a href="https://twitter.com/__semicolon" class="si-twitter" target="_blank"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
                    <li><a href="https://youtube.com/semicolonweb" class="si-youtube" target="_blank"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
                    <li><a href="https://instagram.com/semicolonweb" class="si-instagram" target="_blank"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li> --}}
                    <li><a href="tel:+618364468" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+618364468</span></a></li>
                    <li><a href="mailto:hello@ptpsumut.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">hello@ptpsumut.com</span></a></li>
                </ul>
                <!-- #top-social end -->
            </div>
        </div>
    </div>
</div>