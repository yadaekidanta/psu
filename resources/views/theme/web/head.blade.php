<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="{{config('app.name')}}" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="description" content="PT. Perkebunan Sumatera Utara" />
    <meta name="keywords" content="{{config('app.name') . ': ' .$title ?? config('app.name')}}" />
	<!-- Stylesheets ============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,900&display=swap" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/style.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('semicolon/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/css/magnific-popup.css')}}" type="text/css" />
	<!-- Bootstrap Switch CSS -->
	<link rel="stylesheet" href="{{asset('semicolon/css/components/bs-switches.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('semicolon/css/custom.css')}}" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>
	<!-- Seo Demo Specific Stylesheet -->
	<link rel="stylesheet" href="{{asset('semicolon/css/colors.php?color=FE9603')}}" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="{{asset('semicolon/demos/seo/css/fonts.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('semicolon/demos/seo/seo.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/toastr.css')}}" type="text/css" />
	<meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{config('app.name') . ': ' .$title ?? config('app.name')}}" />
    <meta property="og:url" content="https://ptpsumut.com" />
    <meta property="og:site_name" content="{{config('app.name') . ': ' .$title ?? config('app.name')}}" />
	<link rel="shortcut icon" href="{{asset('icon.png')}}" />
	<!-- / -->
	<!-- Document Title ============================================= -->
	<title>{{config('app.name') . ': ' .$title ?? config('app.name')}}</title>
</head>