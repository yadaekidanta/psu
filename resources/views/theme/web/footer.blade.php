<footer id="footer" class="border-0 bg-white">
    <div class="container">
        <!-- Footer Widgets ============================================= -->
        <div class="footer-widgets-wrap pb-5 clearfix">
            <div class="row col-mb-50">
                <div class="col-md-10">
                    <div class="widget clearfix">
                        <img src="{{asset('icon.png')}}" alt="{{config('app.name')}}" class="alignleft" style="margin-top: 8px; padding-right: 18px; border-right: 1px solid #DDD;">

                        <p>
                            {!!Str::limit('<strong>PT Perkebunan Sumatera Utara (Persero)</strong>
                            merupakan salah satu badan usaha milik daerah Provinsi Sumatera Utara,
                            didirikan berdasarkan Peraturan Daerah Tingkat I Sumatera Utara Nomor 16 Tahun 1979
                            dengan bentuk badan hukum yang pertama sekali berupa Perusahaan Daerah (PD).
                            Kemudian disempurnakan dengan Peraturan Daerah Nomor 24 tahun 1985 yang disahkan
                            dengan Keputusan Menteri Dalam Negeri Nomor 539.22-1434 tanggal 16 Oktober 1985
                            dan diundangkan dalam lembaran daerah Provinsi Sumatera Utara tanggal 29 Januari 1986',400)!!}
                        </p>
                        <div class="line line-sm"></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="widget">
                        <div class="row">
                            <div class="col-12">
                                <div class="feature-box fbox-plain fbox-sm align-items-center">
                                    <div class="fbox-icon">
                                        <i class="icon-phone3 text-dark"></i>
                                    </div>
                                    <div class="fbox-content">
                                        <span class="text-muted">Telpon Kami:</span><br>
                                        <h3 class="nott ls0 fw-semibold">61 8364468</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mt-4">
                                <div class="feature-box fbox-plain fbox-sm align-items-center">
                                    <div class="fbox-icon">
                                        <i class="icon-envelope text-dark"></i>
                                    </div>
                                    <div class="fbox-content">
                                        <span class="text-muted">Email Kami:</span><br>
                                        <h3 class="nott ls0 fw-semibold">hello@ptpsumut.com</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget subscribe-widget clearfix d-none">
                        <h5><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
                        <div class="widget-subscribe-form-result"></div>
                        <form id="widget-subscribe-form" action="include/subscribe.php" method="post" class="mb-0">
                            <div class="input-group mx-auto">
                                <div class="input-group-text bg-transparent"><i class="icon-email2"></i></div>
                                <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
                                <button class="btn btn-success button button-color nott ls0 m-0" type="submit">Subscribe</button>
                            </div>
                        </form>
                    </div>
                    <div class="widget d-none">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-6">
                                <a href="https://facebook.com/semicolonweb" target="_blank" class="social-icon si-dark si-colored si-facebook mb-0" style="margin-right: 10px;">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="https://facebook.com/semicolonweb" target="_blank" class="text-dark"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
                            </div>
                            <div class="col-lg-6 col-md-12 col-6">
                                <a href="https://themeforest.net/user/SemiColonWeb/follow" target="_blank" class="social-icon si-dark si-colored si-rss mb-0" style="margin-right: 10px;">
                                    <i class="icon-rss"></i>
                                    <i class="icon-rss"></i>
                                </a>
                                <a href="https://themeforest.net/user/SemiColonWeb/follow" target="_blank" class="text-dark"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 widget_links">
                    <h5>Profil</h5>
                    <ul>
                        <li><a href="{{route('web.highlight')}}">Highlight</a></li>
                        <li><a href="{{route('web.komisaris_direksi')}}">Komisaris &amp; Direksi</a></li>
                        <li><a href="{{route('web.struktur_organisasi')}}">Struktur Organisasi</a></li>
                        <li><a href="{{route('web.visi_misi')}}">Visi &amp; Misi</a></li>
                        <li><a href="{{route('web.mitra_pelanggan')}}">Mitra &amp; Pelanggan</a></li>
                        <li><a href="{{route('web.sertifikasi_penghargaan')}}">Sertifikasi &amp; Penghargaan</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-6 widget_links">
                    <h5>Media</h5>
                    <ul>
                        <li><a href="{{route('web.berita.index')}}">Berita</a></li>
                        <li><a href="{{route('web.galeri.index')}}">Galeri</a></li>
                        <li><a href="{{route('web.pengumuman.index')}}">Pengumuman</a></li>
                    </ul>
                </div>
                @php
                $produk = \App\Models\Product::limit(5)->get();
                @endphp
                @if ($produk->count() > 0)
                <div class="col-lg-3 col-6 widget_links">
                    <h5>Produk</h5>
                    <ul>
                        @foreach ($produk as $item)
                            <li>
                                <a href="javascript:;">
                                    {{$item->nama}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @php
                $laporan = \App\Models\Laporan::limit(5)->get();
                @endphp
                @if ($laporan->count() > 0)
                <div class="col-lg-2 col-6 widget_links">
                    <h5>Keberlanjutan</h5>
                    <ul>
                        @foreach ($laporan as $lap)
                        <li><a href="{{route('web.laporan.show',$lap->slug)}}">{{$lap->judul}}</a></li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-lg-2 col-6 widget_links">
                    <h5>Lainnya</h5>
                    <ul>
                        <li><a target="_blank" href="https://eproc.ptpsumut.com/">e-Proc</a></li>
                        <li><a href="{{route('web.karir')}}">Karir</a></li>
                        <li><a href="{{route('web.contact')}}">Kontak Kami</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- .footer-widgets-wrap end -->
    </div>
    <!-- Copyrights ============================================= -->
    <div id="copyrights" style="background: url('{{asset('semicolon/demos/seo/images/hero/footer.svg')}}') no-repeat top center; background-size: cover; padding-top: 70px;">
        <div class="container clearfix">
            <div class="row justify-content-between">
                <div class="col-12 col-lg-auto text-center text-lg-start">
                    Hak Cipta &copy; 2019 - {{date('Y')}} Semua Hak Dilindungi oleh {{config('app.name')}}.<br>
                    <div class="copyright-links d-none"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                </div>
                <div class="col-12 col-lg-auto text-center text-lg-end d-none">
                    <div class="copyrights-menu copyright-links clearfix">
                        <a href="#">Home</a>/<a href="#">About Us</a>/<a href="#">Team</a>/<a href="#">Clients</a>/<a href="#">FAQs</a>/<a href="#">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #copyrights end -->
</footer>