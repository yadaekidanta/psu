<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{asset('keenthemes/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('keenthemes/js/scripts.bundle.js')}}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{{asset('js/plugin-office.js')}}"></script>
<script src="{{asset('js/method-office.js')}}"></script>
<!--end::Page Custom Javascript-->
<script>
    counter_notif(localStorage.getItem("route_counter_notif"));
    $(document).on('click', '#notifikasi', function(){
        counter_notif(localStorage.getItem("route_counter_notif"));
        load_notif(localStorage.getItem("route_notification"));
    });
    setInterval(function(){
        counter_notif(localStorage.getItem("route_counter_notif"));
    }, 5000);
    function counter_notif(url){
        // let data = "view="+ view + "&load_keranjang=";
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (response){
                if(response.total_notif > 0){
                    $('#notif_blink').addClass('bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-100 animation-blink');
                }else{
                    $('#notif_blink').removeClass('bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-100 animation-blink');
                }
            }
        });
    }
    function load_notif(url){
        // let data = "view="+ view + "&load_keranjang=";
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (response){
                $('#notif_tender_item').html(response.collection);
                $('#total_notifikasi').html(format_ribuan(response.total_notif) ?? 0);
            },
        });
    }
    localStorage.setItem("route_counter_notif", "{{route('eproc.counter_notif')}}");
    localStorage.setItem("route_notification", "{{route('eproc.notification')}}");
</script>