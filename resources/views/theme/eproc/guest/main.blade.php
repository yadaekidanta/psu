<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	@include('theme.eproc.guest.head')
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="page-bg">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page launcher sidebar-enabled d-flex flex-row flex-column-fluid me-lg-5" id="kt_page">
				{{$slot}}
			</div>
			<!--end::Page-->
			<!--begin::Modals-->
			<!--end::Modals-->
		</div>
		<!--end::Main-->
		<!--begin::Javascript-->
		@include('theme.eproc.guest.js')
		@include('theme.eproc.guest.modal')
        @yield('custom_js')
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>