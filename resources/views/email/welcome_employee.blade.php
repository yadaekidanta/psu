<style>html,body { padding: 0; margin:0; }</style>
<div style="font-family:Arial,Helvetica,sans-serif; line-height: 1.5; font-weight: normal; font-size: 15px; color: #2F3044; min-height: 100%; margin:0; padding:0; width:100%; background-color:#edf2f7">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0 auto; padding:0; max-width:600px">
		<tbody>
			<tr>
				<td align="center" valign="center" style="text-align:center; padding: 40px">
					<a href="https://ptpsumut.com" rel="noopener" target="_blank">
						<img src="https://ptpsumut.com/icon.png" style="height: 45px" alt="logo">
							<tr>
								<td align="left" valign="center">
									<div style="text-align:left; margin: 0 20px; padding: 40px; background-color:#ffffff; border-radius: 6px">
										<!--begin:Email content-->
										<div style="padding-bottom: 30px; font-size: 17px;">
											<strong>Hai {{$user->nama}},</strong>
										</div>
										<div style="padding-bottom: 20px">Akun anda berhasil di daftarkan menggunakan password yang Anda request di Google form.</div>
										<!--end:Email content-->
										<div style="padding-bottom: 10px">Salam Hangat,
                                        <br>PT. Perkebunan Sumatera Utara.
										<tr>
											<td align="center" valign="center" style="font-size: 13px; text-align:center;padding: 20px; color: #6d6e7c;">
												<p>
													Jalan Letjend Jamin Ginting KM 13, no. 45 Medan<br>
													Kelurahan Lau Cih, Kecamatan Medan Tuntungan<br>
													Kota Medan.
												</p>
												<p>
                                                    Copyright ©
                                                    <a href="https://ptpsumut.com" rel="noopener" target="_blank">PT. Perkebunan Sumatera Utara.</a>.
                                                </p>
											</td>
										</tr></br></div>
									</div>
								</td>
							</tr>
						</img>
					</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>