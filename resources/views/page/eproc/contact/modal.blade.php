<div class="modal-header header-bg">
    <h2 class="text-white">{{config('app.name')}}
    <small class="ms-2 fs-7 fw-normal text-white opacity-50">Kontak Kami</small></h2>
    <div class="btn btn-sm btn-icon btn-color-white btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y m-5">
    <form class="mx-auto mw" novalidate="novalidate" id="form_kontak">
        <div class="w-100">
            <div class="pb-10 pb-lg-12 d-none">
                <h2 class="fw-bolder text-dark">Business Details</h2>
                <div class="text-muted fw-bold fs-6">If you need more info, please check out
                <a href="#" class="link-primary fw-bolder">Help Page</a>.</div>
            </div>
            <div class="row">
                <div class="col-4">
                    <label class="form-label required">Nama</label>
                    <input name="nama" type="text" class="form-control form-control-lg form-control-solid" />
                </div>
                <div class="col-4">
                    <label class="form-label required">Email</label>
                    <input name="email" type="email" class="form-control form-control-lg form-control-solid" />
                </div>
                <div class="col-4">
                    <label class="form-label required">No HP</label>
                    <input name="hp" type="tel" class="form-control form-control-lg form-control-solid" />
                </div>
                <div class="col-8 pt-3">
                    <label class="form-label required">Subject</label>
                    <input name="subject" type="text" class="form-control form-control-lg form-control-solid" />
                </div>
                <div class="col-12 pt-3">
                    <label class="form-label required">Pesan</label>
                    <textarea name="pesan" class="form-control form-control-lg form-control-solid"></textarea>
                </div>
            </div>
            <div class="d-flex flex-stack pt-5">
                <div>
                    <button type="button" class="btn btn-lg btn-primary me-3" id="tombol_kirim_pesan" onclick="handle_save_modal('#tombol_kirim_pesan','#form_kontak','{{route('eproc.contact.store')}}','POST','#modalPage');">
                        <span class="indicator-label">
                            Kirim
                            <span class="svg-icon svg-icon-3 ms-2 me-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
                                    <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
                                </svg>
                            </span>
                        </span>
                        <span class="indicator-progress">Harap tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>