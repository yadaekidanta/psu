<x-auth-layout title="Halaman Setel Ulang Sandi">
    <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
        <form class="form w-100" novalidate="novalidate" id="form_reset">
            <div class="text-center mb-10">
                <h1 class="text-dark mb-3">{{config('app.name')}}</h1>
            </div>
            <div class="fv-row mb-10">
                <input type="hidden" name="token" value="{{$token}}">
                <label class="form-label fs-6 fw-bolder text-dark">Kata sandi baru</label>
                <input class="form-control form-control-lg form-control-solid" type="password" id="password" name="password" autocomplete="off" data-reset="1" />
            </div>
            <div class="fv-row mb-10">
                <label class="form-label fs-6 fw-bolder text-dark">Kata sandi baru</label>
                <input class="form-control form-control-lg form-control-solid" type="password" id="password_confirmation" name="password_confirmation" autocomplete="off" data-reset="2" />
            </div>
            <div class="text-center">
                <button type="button" onclick="handle_post('#tombol_reset','#form_reset','{{route('office.auth.reset')}}');" id="tombol_reset" class="btn btn-lg btn-success w-100 mb-5" data-login="3">
                    <span class="indicator-label">Lanjutkan</span>
                    <span class="indicator-progress">Harap tunggu...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </form>
    </div>
    @section('custom_js')
    <script>
        $("#password").focus();
    </script> 
    @endsection
</x-auth-layout>