<x-auth-layout title="Halaman Masuk">
    <div id="page_login">
        <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
            <form class="form w-100" novalidate="novalidate" id="form_login">
                <div class="text-center mb-10">
                    <h4 class="text-dark mb-3">Procurement {{config('app.name')}}</h3>
                    <div class="text-gray-400 fw-bold fs-4">
                        Belum terdaftar?
                        <a href="javascript:;" onclick="auth_content('page_register');" class="link-primary fw-bolder">Buat akun</a>
                    </div>
                </div>
                <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                    <input class="form-control form-control-lg form-control-solid" type="email" id="email" name="email" autocomplete="off" data-login="1" />
                </div>
                <div class="fv-row mb-10">
                    <div class="d-flex flex-stack mb-2">
                        <label class="form-label fw-bolder text-dark fs-6 mb-0">Kata Sandi</label>
                        <a href="javascript:;" onclick="auth_content('page_forgot');" class="link-success fs-6 fw-bolder">Lupa kata sandi ?</a>
                    </div>
                    <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" data-login="2" />
                </div>
                <div class="text-center">
                    <button type="button" onclick="handle_post('#tombol_login','#form_login','{{route('eproc.auth.login')}}');" id="tombol_login" class="btn btn-lg btn-success w-100 mb-5" data-login="3">
                        <span class="indicator-label">Lanjutkan</span>
                        <span class="indicator-progress">Harap tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="page_register">
        <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
            <form class="form w-100" novalidate="novalidate" id="form_register">
                <div class="text-center mb-10">
                    <h4 class="text-dark mb-3">Procurement {{config('app.name')}}</h3>
                    <div class="text-gray-400 fw-bold fs-4">
                        Sudah terdaftar?
                        <a href="javascript:;" onclick="auth_content('page_login');" class="link-primary fw-bolder">Masuk disini</a>
                    </div>
                </div>
                {{-- <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">NPWP</label>
                    <input class="form-control form-control-lg form-control-solid" type="tel" id="npwp" name="npwp" autocomplete="off" data-register="1" />
                </div> --}}
                <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">Nama Lengkap</label>
                    <input class="form-control form-control-lg form-control-solid" type="text" id="nama" name="nama" autocomplete="off" data-register="1" />
                </div>
                <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">Nama Perusahaan</label>
                    <input class="form-control form-control-lg form-control-solid" type="text" id="nama_perusahaan" name="nama_perusahaan" autocomplete="off" data-register="2" />
                </div>
                <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                    <input class="form-control form-control-lg form-control-solid" type="email" id="email" name="email" autocomplete="off" data-register="3" />
                </div>
                <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">No Handphone</label>
                    <input class="form-control form-control-lg form-control-solid" type="tel" id="hp" name="hp" autocomplete="off" data-register="4" />
                </div>
                <div class="fv-row mb-10">
                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Kata Sandi</label>
                    <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" data-register="5" />
                </div>
                <div class="text-center">
                    <button type="button" onclick="handle_post('#tombol_register','#form_register','{{route('eproc.auth.register')}}');" id="tombol_register" class="btn btn-lg btn-success w-100 mb-5" data-register="6">
                        <span class="indicator-label">Lanjutkan</span>
                        <span class="indicator-progress">Harap tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="page_forgot">
        <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
            <form class="form w-100" novalidate="novalidate" id="form_forgot">
                <div class="text-center mb-10">
                    <h4 class="text-dark mb-3">Procurement {{config('app.name')}}</h3>
                </div>
                <div class="fv-row mb-10">
                    <div class="d-flex flex-stack mb-2">
                        <label class="form-label fw-bolder text-dark fs-6 mb-0">Email</label>
                        <a href="javascript:;" onclick="auth_content('page_login');" class="link-success fs-6 fw-bolder">Ingat kata sandi ?</a>
                    </div>
                    <input class="form-control form-control-lg form-control-solid" type="email" id="mail" name="email" autocomplete="off" data-forgot="1" />
                </div>
                <div class="text-center">
                    <button type="button" onclick="handle_post('#tombol_forgot','#form_forgot','{{route('eproc.auth.forgot')}}');" id="tombol_forgot" class="btn btn-lg btn-success w-100 mb-5" data-forgot="2">
                        <span class="indicator-label">Lanjutkan</span>
                        <span class="indicator-progress">Harap tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    @section('custom_js')
    <script>
        auth_content('page_login');
        number_only('npwp');
        npwp_format('npwp');
    </script> 
    @endsection
</x-auth-layout>