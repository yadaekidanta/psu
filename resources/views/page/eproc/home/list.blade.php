@foreach ($collection as $item)
<div class="timeline-item">
    <div class="timeline-line w-40px"></div>
    <div class="timeline-icon symbol symbol-circle symbol-40px me-4">
        <div class="symbol-label">
            <span class="svg-icon svg-icon-2 svg-icon-white">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path opacity="0.3" d="M2 4V16C2 16.6 2.4 17 3 17H13L16.6 20.6C17.1 21.1 18 20.8 18 20V17H21C21.6 17 22 16.6 22 16V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4Z" fill="black" />
                    <path d="M18 9H6C5.4 9 5 8.6 5 8C5 7.4 5.4 7 6 7H18C18.6 7 19 7.4 19 8C19 8.6 18.6 9 18 9ZM16 12C16 11.4 15.6 11 15 11H6C5.4 11 5 11.4 5 12C5 12.6 5.4 13 6 13H15C15.6 13 16 12.6 16 12Z" fill="black" />
                </svg>
            </span>
        </div>
    </div>
    <div class="timeline-content mb-10 mt-n1">
        <div class="pe-3 mb-5">
            <div class="fs-5 fw-bold mb-2">
                <a class="text-white" href="javascript:;">{{$item->judul}}</a>
            </div>
            <div class="d-flex align-items-center mt-1 fs-6">
                <div class="text-white opacity-50 me-2 fs-7">
                    {{$item->created_at->diffForHumans()}}
                    {{-- {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}} <br>
                    Jam {{$item->created_at->isoFormat('h A')}} --}}
                </div>
                {{-- <div class="symbol symbol-circle symbol-25px" data-bs-toggle="tooltip" data-bs-boundary="window" data-bs-placement="top" title="Nina Nilson">
                    <img src="assets/media/avatars/150-11.jpg" alt="img" />
                </div> --}}
                <a href="javascript:;" class="text-success fs-7 fw-bolder">Rp. {{number_format($item->nilai_tender)}}</a>
            </div>
        </div>
    </div>
</div>
@endforeach