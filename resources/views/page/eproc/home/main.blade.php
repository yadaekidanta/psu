<x-eproc-guest-layout title="Selamat datang">
    <div class="d-flex flex-row-fluid">
        <div class="d-flex flex-column flex-row-fluid align-items-center">
            <div class="d-flex flex-column flex-column-fluid mb-5 mb-lg-10">
                <div class="d-flex flex-center pt-10 pt-lg-0 mb-10 mb-lg-0 h-lg-225px">
                    <div class="btn btn-icon btn-active-color-primary w-30px h-30px d-lg-none me-4 ms-n15" id="kt_sidebar_toggle">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                                <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                            </svg>
                        </span>
                    </div>
                    <a href="{{route('eproc.home')}}">
                        <img alt="Logo" src="{{asset('icon.png')}}" class="h-75px h-lg-80px" />
                    </a>
                </div>
                <div class="row g-7 w-xxl-850px">
                    <div class="col-xxl-5">
                        <div class="card border-0 shadow-none h-lg-100" style="background-color: #A838FF">
                            <div class="card-body d-flex flex-column flex-center pb-0 pt-15">
                                <h3 class="text-white mb-2 fw-boldest text-center text-uppercase mb-6">e-Procurement {{config('app.name')}}</h3>
                                <div class="px-10 mb-10">
                                    <div class="mb-7">
                                        <div class="d-flex align-items-center mb-2">
                                            <span class="svg-icon svg-icon-4 svg-icon-white opacity-75 me-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <path d="M14.4 11H3C2.4 11 2 11.4 2 12C2 12.6 2.4 13 3 13H14.4V11Z" fill="black" />
                                                    <path opacity="0.3" d="M14.4 20V4L21.7 11.3C22.1 11.7 22.1 12.3 21.7 12.7L14.4 20Z" fill="black" />
                                                </svg>
                                            </span>
                                            <span class="text-white opacity-75">Easy Tool</span>
                                        </div>
                                        <div class="d-flex align-items-center mb-2">
                                            <span class="svg-icon svg-icon-4 svg-icon-white opacity-75 me-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <path d="M14.4 11H3C2.4 11 2 11.4 2 12C2 12.6 2.4 13 3 13H14.4V11Z" fill="black" />
                                                    <path opacity="0.3" d="M14.4 20V4L21.7 11.3C22.1 11.7 22.1 12.3 21.7 12.7L14.4 20Z" fill="black" />
                                                </svg>
                                            </span>
                                            <span class="text-white opacity-75">Fast Reports</span>
                                        </div>
                                    </div>
                                    <a href="{{route('eproc.dashboard')}}" class="btn btn-hover-rise text-white bg-white bg-opacity-10 text-uppercase fs-7 fw-bolder">Go To Dashboard</a>
                                </div>
                                <img class="mw-100 h-225px mx-auto mb-lg-n18" src="{{asset('keenthemes/media/illustrations/sigma-1/12.png')}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-7">
                        <div class="row g-lg-7">
                            <div class="col-sm-6">
                                <a href="{{route('eproc.tender.index')}}" class="card border-0 shadow-none min-h-200px mb-7" style="background-color: #F9666E">
                                    <div class="card-body d-flex flex-column flex-center text-center">
                                        <img class="mw-100 h-100px mb-7 mx-auto" src="{{asset('keenthemes/media/illustrations/sigma-1/4.png')}}" />
                                        <h4 class="text-white fw-bolder text-uppercase">Cari Pengadaan</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="{{route('eproc.auth.index')}}" class="card border-0 shadow-none min-h-200px mb-7" style="background-color: #35D29A">
                                    <div class="card-body d-flex flex-column flex-center text-center">
                                        <img class="mw-100 h-100px mb-7 mx-auto" src="{{asset('keenthemes/media/illustrations/sigma-1/5.png')}}" />
                                        <h4 class="text-white fw-bolder text-uppercase">Daftar</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="card border-0 shadow-none min-h-200px" style="background-color: #D5D83D">
                            <div class="card-body d-flex flex-center flex-wrap">
                                <img class="mw-100 h-200px me-4 mb-5 mb-lg-0" src="{{asset('keenthemes/media/illustrations/sigma-1/11.png')}}" />
                                <div class="d-flex flex-column align-items-center align-items-md-start flex-grow-1">
                                    <h3 class="text-gray-900 fw-boldest text-uppercase mb-5">Panduan</h3>
                                    <div class="text-gray-800 mb-5 text-center text-md-start">Explore our powerful
                                    <br />procurement</div>
                                    <a href="javascript:;" onclick="handle_open_modal('{{route('eproc.panduan')}}','#modalPage','#contentListResult');" class="btn btn-hover-rise text-gray-900 text-uppercase fs-7 fw-bolder" style="background-color: #EBEE51">Pelajari lebih lanjut</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column-auto flex-center">
                <ul class="menu fw-bold order-1">
                    <li class="menu-item">
                        <a href="javascript:;" onclick="handle_open_modal('{{route('eproc.tentang')}}','#modalPage','#contentListResult');" class="menu-link text-white opacity-50 opacity-100-hover px-3">Tentang</a>
                    </li>
                    <li class="menu-item">
                        <a href="javascript:;" onclick="handle_open_modal('{{route('eproc.contact')}}','#modalPage','#contentListResult');" class="menu-link text-white opacity-50 opacity-100-hover px-3">Kontak</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="kt_sidebar" class="sidebar px-5 py-5 py-lg-8 px-lg-11" data-kt-drawer="true" data-kt-drawer-name="sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="375px" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_sidebar_toggle">
        <div class="d-flex flex-stack mb-5 mb-lg-8" id="kt_sidebar_header">
            <h2 class="text-white">Daftar Pengadaan Terbaru</h2>
            <div class="ms-1 d-none">
                <button class="btn btn-icon btn-sm btn-color-white btn-active-color-primary me-n5" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="black" />
                            <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
                            <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
                            <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="black" />
                        </svg>
                    </span>
                </button>
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
                    <div class="menu-item px-3">
                        <div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
                    </div>
                    <div class="separator mb-3 opacity-75"></div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">New Ticket</a>
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">New Customer</a>
                    </div>
                    <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                        <a href="#" class="menu-link px-3">
                            <span class="menu-title">New Group</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="menu-sub menu-sub-dropdown w-175px py-4">
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3">Admin Group</a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3">Staff Group</a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3">Member Group</a>
                            </div>
                        </div>
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">New Contact</a>
                    </div>
                    <div class="separator mt-3 opacity-75"></div>
                    <div class="menu-item px-3">
                        <div class="menu-content px-3 py-3">
                            <a class="btn btn-primary btn-sm px-4" href="#">Generate Reports</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-5 mb-lg-8" id="kt_sidebar_body">
            <div class="hover-scroll-y me-n6 pe-6" id="kt_sidebar_body" data-kt-scroll="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_sidebar_header, #kt_sidebar_footer" data-kt-scroll-wrappers="#kt_page, #kt_sidebar, #kt_sidebar_body" data-kt-scroll-offset="0">
                <div class="timeline">
                    <div id="content_list">
                        <div id="list_result"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" id="kt_sidebar_footer">
            <a href="{{route('eproc.tender.index')}}" class="btn btn-hover-rise text-white bg-white bg-opacity-10 text-uppercase fs-7 fw-bolder">Lihat Semua Tender</a>
        </div>
    </div>
    @section('custom_js')
        <script>
            load_list(1);                
            setInterval(function(){
                load_list(1);                
            }, 10000);
        </script>
    @endsection
</x-eproc-guest-layout>