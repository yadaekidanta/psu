<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Tender</th>
            <th class="min-w-125px">Nilai Tender</th>
            <th class="min-w-125px">Nilai Penawaran Saya</th>
            <th class="min-w-125px">Status Tender</th>
            <th class="min-w-125px">Status Penawaran</th>
            <th class="min-w-125px">Tanggal Buat Penawaran</th>
            <th class="min-w-125px"></th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $item)
        <tr>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->tender->judul}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">Rp. {{number_format($item->tender->nilai_tender)}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">Rp. {{number_format($item->nilai)}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->tender->st}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->st}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->created_at->diffForHumans()}} <br>
                    {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
                </a>
            </td>
            <td>
            @if ($item->st == "Penawaran diterima")
                @if ($item->tender->attachment_spk)
                <a href="{{route('eproc.tender.download_spk',$item->tender->slug)}}" class="btn btn-sm btn-primary"><i class="la la-download"></i>Unggah File SPK</a>
                @endif
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}