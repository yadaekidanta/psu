
<div class="toolbar d-flex flex-stack mb-3 mb-lg-5" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-5 py-2">
            <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Buat Penawaran</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('office.dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Tender</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">Buat Penawaran {{$data->judul}}</li>
            </ul>
        </div>
        <div class="d-flex align-items-center py-2">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-info">Kembali</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-body pb-5">
                <form id="form_input">
                    <div class="row">
                        <input type="hidden" name="tender_id" value="{{$data->id}}">
                        <div class="col-lg-7">
                            <label class="fs-6 fw-bold mb-2">Judul</label>
                            <input type="text" class="form-control" name="judul" placeholder="Masukkan judul..." value="{{$data->judul}}" readonly>
                        </div>
                        <div class="col-lg-2">
                            <label class="{{$data->id ? '' : 'required'}} fs-6 fw-bold mb-2">File KAK</label>
                            <br>
                            <a class="btn btn-sm btn-warning text-black" href="{{route('eproc.tender.download',$data->slug)}}"><i class="la la-download"></i></a>
                        </div>
                        <div class="col-lg-3">
                            <label class="fs-6 fw-bold mb-2">Nilai Tender</label>
                            <input type="text" class="form-control" id="nilai_tender" name="nilai_tender" value="{{$data->nilai_tender ? number_format($data->nilai_tender): 0}}" readonly>
                        </div>
                        <div class="col-lg-4 mt-5">
                            <label class="fs-6 fw-bold mb-2">Tanggal Akhir Pendaftaran Tender</label>
                            <input type="text" class="form-control" id="tender_deadline" name="tender_deadline" readonly value="{{$data->tender_deadline_at->format('j F Y')}}">
                        </div>
                        <div class="col-lg-4 mt-5">
                            <label class="fs-6 fw-bold mb-2">Tanggal Akhir Pengerjaan Tender</label>
                            <input type="text" class="form-control" id="work_deadline" name="work_deadline" readonly value="{{$data->work_deadline_at->format('j F Y')}}">
                        </div>
                        <div class="col-lg-6 mt-5">
                            <label class="required fs-6 fw-bold mb-2">Nilai Penawaran Anda</label>
                            <input type="tel" class="form-control" id="nilai_penawaran" name="nilai_penawaran">
                        </div>
                        <div class="col-lg-6 mt-5">
                            <label class="required fs-6 fw-bold mb-2">File Penawaran Anda</label>
                            <input type="file" class="form-control" id="file" name="file">
                        </div>
                        <div class="col-lg-12 mt-5 mb-10">
                            <label class="required fs-6 fw-bold mb-2">Pesan</label>
                            <textarea class="form-control" name="pesan"></textarea>
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('eproc.my-tender.store')}}','POST');" class="btn btn-sm btn-warning text-black">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('nilai_penawaran');
    ribuan('nilai_penawaran');
</script>