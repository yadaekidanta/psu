<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr>
            <th>Jenis Dokumen</th>
            <th>Nomor</th>
            <th>Tanggal Unggah</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($dokumen as $item)
        <tr>
            <td>{{$item->jenis->nama}}</td>
            <td>{{$item->nomor}}</td>
            <td>
                {{$item->created_at->diffForHumans()}} <br>
                {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
            </td>
            <td>
                <a href="{{route('eproc.doc.download',$item->id)}}" class="btn btn-sm btn-primary"><i class="la la-download"></i>Unggah</a>
                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('eproc.doc.destroy',$item->id)}}');" class="btn btn-sm btn-danger"><i class="la la-trash"></i>Hapus</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>