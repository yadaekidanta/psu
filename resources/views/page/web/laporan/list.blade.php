<div class="row grid-container" data-layout="masonry" style="overflow: visible">
@foreach ($collection as $item)
<div class="col-lg-4 mb-4">
    <div class="flip-card text-center top-to-bottom">
        <div class="flip-card-front bg-warning dark" data-height-xl="200">
            <div class="flip-card-inner">
                <div class="card bg-transparent border-0 text-center">
                    <div class="card-body">
                        <h3 class="card-title">Laporan Tahunan {{$item->tahun}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="flip-card-back bg-success dark" data-height-xl="200">
            <div class="flip-card-inner">
                <p class="mb-2 text-white">{{$item->judul}}</p>
                <a href="{{route('web.laporan.show',$item->slug)}}" class="btn btn-outline-light mt-2">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
</div>
@endforeach
</div>