<div class="row grid-container" data-layout="masonry" style="overflow: visible">
    @foreach ($collection as $item)
    @php
    $isi = strip_tags($item->deskripsi);
    @endphp
    <div class="col-lg-4 mb-4">
        <div class="flip-card text-center top-to-bottom">
            <div class="flip-card-front bg-warning dark" data-height-xl="200">
                <div class="flip-card-inner">
                    <div class="card bg-transparent border-0 text-center">
                        <div class="card-body">
                            <h3 class="card-title">{{$item->nama}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flip-card-back bg-success dark" data-height-xl="200" style="background-image: url('{{$item->image}}');">
                <div class="flip-card-inner">
                    <p class="mb-2 text-white">{{Str::limit($isi,200,'...')}}</p>
                    <a href="{{route('web.product.show',$item->slug)}}" class="btn btn-outline-light mt-2">Baca Selengkapnya</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    </div>