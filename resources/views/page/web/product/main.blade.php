<x-web-layout title="Produk">
    <section id="page-title">
        <div class="container clearfix">
            <h1>Produk</h1>
            <span></span>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('web.home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Produk</li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div id="content_list">
                    <div id="list_result"></div>
                </div>
            </div>
        </div>
    </section>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-web-layout>