<x-web-layout title="Produk - {{$product->nama}}">
    <section id="page-title">
        <div class="container clearfix">
            <h1>Produk</h1>
            <span></span>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('web.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Media</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Produk</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$product->judul}}</li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row gutter-40 col-mb-80">
                    <div class="postcontent col-lg-12">
                        <div class="single-post mb-0">
                            <div class="entry clearfix">
                                <div class="entry-title">
                                    <h2>{{$product->nama}}</h2>
                                </div>
                                <div class="entry-meta">
                                    <ul>
                                        <li><i class="icon-calendar3"></i> {{$product->created_at->isoFormat('dddd,')}} {{$product->created_at->format('j F Y')}} <br></li>
                                        <li><i class="icon-folder-open"></i> <a href="#">{{Str::title($product->tipe)}}</a></li>
                                    </ul>
                                </div>
                                <div class="entry-content mt-0">
                                    <div class="entry-image alignleft">
                                        <a href="javascript:;">
                                            <img src="{{$product->image}}" alt="{{$product->nama}}">
                                        </a>
                                    </div>
                                    {!!$product->deskripsi!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>