<x-web-layout title="Galeri">
    <section id="page-title">
        <div class="container clearfix">
            <h1>Galeri</h1>
            <span></span>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('web.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Media</a></li>
                <li class="breadcrumb-item active" aria-current="page">Galeri</li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row gutter-40 col-mb-80">
                    <div class="postcontent col-lg-8">
                        <div id="posts" class="row gutter-40">
                            <div id="content_list">
                                <div id="list_result"></div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-lg-4">
                        <div class="sidebar-widgets-wrap">
                            <div class="widget clearfix">
                                <div class="tabs mb-0 clearfix" id="sidebar-tabs">
                                    <ul class="tab-nav clearfix">
                                        <li><a href="#berita">Berita</a></li>
                                        <li><a href="#pengumuman">Pengumuman</a></li>
                                    </ul>
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="berita">
                                            <div class="posts-sm row col-mb-30" id="popular-post-list-sidebar">
                                                @foreach ($berita as $item)
                                                <div class="entry col-12">
                                                    <div class="grid-inner row g-0">
                                                        <div class="col-auto">
                                                            <div class="entry-image">
                                                                <a href="{{route('web.berita.show',$item->slug)}}">
                                                                    <img class="rounded-circle" src="{{$item->image}}" alt="{{$item->judul}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col ps-3">
                                                            <div class="entry-title">
                                                                <h4>
                                                                    <a href="{{route('web.berita.show',$item->slug)}}">{{$item->judul}}</a>
                                                                </h4>
                                                            </div>
                                                            <div class="entry-meta">
                                                                <ul>
                                                                    <li>{{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-content clearfix" id="pengumuman">
                                            <div class="posts-sm row col-mb-30" id="recent-post-list-sidebar">
                                                @foreach ($pengumuman as $item)
                                                <div class="entry col-12">
                                                    <div class="grid-inner row g-0">
                                                        <div class="col-auto">
                                                            <div class="entry-image">
                                                                <a href="{{route('web.pengumuman.show',$item->slug)}}">
                                                                    <img class="rounded-circle" src="{{$item->image}}" alt="{{$item->judul}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col ps-3">
                                                            <div class="entry-title">
                                                                <h4>
                                                                    <a href="{{route('web.pengumuman.show',$item->slug)}}">{{$item->judul}}</a>
                                                                </h4>
                                                            </div>
                                                            <div class="entry-meta">
                                                                <ul>
                                                                    <li>{{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-web-layout>