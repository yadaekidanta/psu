<x-web-layout title="Highlight">
    <section id="content">
        <div class="content-wrap pt-0">
            <!-- Features ============================================= -->
            <div class="section bg-transparent mt-4 mb-0 pb-0">
                <div class="container">
                    <div class="heading-block border-bottom-0 center mx-auto mb-0" style="max-width: 550px">
                        <h3 class="nott ls0 mb-3">Highlight</h3>
                    </div>
                    {!!$konten->isi!!}
                </div>
            </div>
        </div>
    </section>
</x-web-layout>