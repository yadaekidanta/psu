<x-web-layout title="Komisaris & Direksi">
    <section id="content">
        <div class="content-wrap pt-0">
            <div class="container clearfix">
                <div class="heading-block border-bottom-0 center mx-auto mt-5 mb-0" style="max-width: 550px">
                    <h3 class="nott ls0 mb-3">Komisaris {{config('app.name')}}</h3>
                </div>
                <div class="row col-mb-50 mb-0">
                    @foreach ($komisaris as $item)
                    <div class="col-lg-6">
                        <div class="team team-list row align-items-center">
                            <div class="team-image col-sm-6">
                                <img src="{{$item->image}}" alt="{{$item->nama}}">
                            </div>
                            <div class="team-desc col-sm-6">
                                <div class="team-title"><h4>{{$item->nama}}</h4><span>{{$item->role->nama}}</span></div>
                                {{-- <div class="team-content">
                                    <p>Carbon emissions reductions giving, legitimize amplify non-partisan Aga Khan. Policy dialogue assessment expert free-speech cornerstone disruptor freedom. Cesar Chavez empower.</p>
                                </div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="heading-block border-bottom-0 center mx-auto mt-5 mb-0" style="max-width: 550px">
                    <h3 class="nott ls0 mb-3">Direksi {{config('app.name')}}</h3>
                </div>
                <div id="oc-team" class="owl-carousel team-carousel bottommargin carousel-widget" data-margin="30" data-pagi="false" data-items-sm="2" data-items-md="2" data-items-xl="3">
                    @foreach ($direksi as $item)
                    <div class="oc-item">
                        <div class="team">
                            <div class="team-image">
                                <img src="{{$item->image}}" alt="{{$item->nama}}">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>{{$item->nama}}</h4><span>{{$item->role->nama}}</span></div>
                                {{-- <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</x-web-layout>