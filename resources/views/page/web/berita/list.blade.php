@foreach ($collection as $item)
@php
$isi = strip_tags($item->isi);
@endphp
<div class="entry col-12">
    <div class="grid-inner row g-0">
        <div class="col-md-4">
            <div class="entry-image">
                <a href="{{route('web.berita.show',$item->slug)}}">
                    <img src="{{$item->image}}" alt="{{$item->judul}}">
                </a>
            </div>
        </div>
        <div class="col-md-8 ps-md-4">
            <div class="entry-title title-sm">
                <h2><a href="{{route('web.berita.show',$item->slug)}}">{{$item->judul}}</a></h2>
            </div>
            <div class="entry-meta">
                <ul>
                    <li><i class="icon-calendar3"></i> {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}</li>
                    <li><i class="icon-folder-open"></i><a href="javascript:;">{{Str::title($item->tipe)}}</a></li>
                </ul>
            </div>
            <div class="entry-content">
                {{Str::limit($isi, 200, '...')}}
                <a href="{{route('web.berita.show',$item->slug)}}" class="more-link">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
</div>
@endforeach
{{$collection->links('theme.web.pagination')}}