<x-web-layout title="Berita - {{$media->judul}}">
    <section id="page-title">
        <div class="container clearfix">
            <h1>Berita</h1>
            <span></span>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('web.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Media</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">Berita</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$media->judul}}</li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row gutter-40 col-mb-80">
                    <div class="postcontent col-lg-8">
                        <div class="single-post mb-0">
                            <div class="entry clearfix">
                                <div class="entry-title">
                                    <h2>{{$media->judul}}</h2>
                                </div>
                                <div class="entry-meta">
                                    <ul>
                                        <li><i class="icon-calendar3"></i> {{$media->created_at->isoFormat('dddd,')}} {{$media->created_at->format('j F Y')}} <br></li>
                                        <li><i class="icon-folder-open"></i> <a href="#">{{Str::title($media->tipe)}}</a></li>
                                    </ul>
                                </div>
                                <div class="entry-content mt-0">
                                    <div class="entry-image alignleft">
                                        <a href="javascript:;">
                                            <img src="{{$media->image}}" alt="{{$media->judul}}">
                                        </a>
                                    </div>
                                    {!!$media->isi!!}
                                </div>
                            </div>
                            <div class="line"></div>
                            <h4>Berita Terkait:</h4>
                            <div class="related-posts row posts-md col-mb-30">
                                @foreach ($related as $item)
                                <div class="entry col-12 col-md-6">
                                    <div class="grid-inner row align-items-center gutter-20">
                                        <div class="col-4">
                                            <div class="entry-image">
                                                <a href="{{route('web.berita.show',$item->slug)}}">
                                                    <img src="{{$item->image}}" alt="{{$item->judul}}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="entry-title title-xs">
                                                <h3>
                                                    <a href="{{route('web.berita.show',$item->slug)}}">{{$item->judul}}</a>
                                                </h3>
                                            </div>
                                            <div class="entry-meta">
                                                <ul>
                                                    <li><i class="icon-calendar3"></i> {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}} <br></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="sidebar col-lg-4">
                        <div class="sidebar-widgets-wrap">
                            <div class="widget clearfix">
                                <div class="tabs mb-0 clearfix" id="sidebar-tabs">
                                    <ul class="tab-nav clearfix">
                                        <li><a href="#galeri">Galeri</a></li>
                                        <li><a href="#pengumuman">Pengumuman</a></li>
                                    </ul>
                                    <div class="tab-container">
                                        <div class="tab-content clearfix" id="galeri">
                                            <div class="posts-sm row col-mb-30" id="popular-post-list-sidebar">
                                                @foreach ($galeri as $item)
                                                <div class="entry col-12">
                                                    <div class="grid-inner row g-0">
                                                        <div class="col-auto">
                                                            <div class="entry-image">
                                                                <a href="{{route('web.galeri.show',$item->slug)}}">
                                                                    <img class="rounded-circle" src="{{$item->image}}" alt="{{$item->judul}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col ps-3">
                                                            <div class="entry-title">
                                                                <h4>
                                                                    <a href="{{route('web.galeri.show',$item->slug)}}">{{$item->judul}}</a>
                                                                </h4>
                                                            </div>
                                                            <div class="entry-meta">
                                                                <ul>
                                                                    <li>{{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-content clearfix" id="pengumuman">
                                            <div class="posts-sm row col-mb-30" id="recent-post-list-sidebar">
                                                @foreach ($pengumuman as $item)
                                                <div class="entry col-12">
                                                    <div class="grid-inner row g-0">
                                                        <div class="col-auto">
                                                            <div class="entry-image">
                                                                <a href="{{route('web.pengumuman.show',$item->slug)}}">
                                                                    <img class="rounded-circle" src="{{$item->image}}" alt="{{$item->judul}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col ps-3">
                                                            <div class="entry-title">
                                                                <h4>
                                                                    <a href="{{route('web.pengumuman.show',$item->slug)}}">{{$item->judul}}</a>
                                                                </h4>
                                                            </div>
                                                            <div class="entry-meta">
                                                                <ul>
                                                                    <li>{{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>