<x-web-layout title="Kontak">
    <section id="page-title">
        <div class="container clearfix">
            <h1>Kontak</h1>
            <span></span>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('web.home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Kontak</li>
            </ol>
        </div>
    </section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row gutter-40 col-mb-80">
                    <div class="postcontent col-lg-9">
                        <div class="form-widget">
                            <div class="form-result"></div>
                            <form class="mb-0" id="template-contactform">
                                <div class="form-process">
                                    <div class="css3-spinner">
                                        <div class="css3-spinner-scaler"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="template-contactform-name">Nama <small>*</small></label>
                                        <input type="text" id="template-contactform-name" name="nama" value="" class="sm-form-control required" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="template-contactform-email">Email <small>*</small></label>
                                        <input type="email" id="template-contactform-email" name="email" value="" class="required email sm-form-control" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="template-contactform-phone">No HP</label>
                                        <input type="text" id="template-contactform-phone" name="hp" value="" class="required sm-form-control" />
                                    </div>
                                    <div class="w-100"></div>
                                    <div class="col-md-8 form-group">
                                        <label for="template-contactform-subject">Subject <small>*</small></label>
                                        <input type="text" id="template-contactform-subject" name="subject" value="" class="required sm-form-control" />
                                    </div>
                                    <div class="w-100"></div>
                                    <div class="col-12 form-group">
                                        <label for="template-contactform-message">Pesan <small>*</small></label>
                                        <textarea class="required sm-form-control" id="template-contactform-message" name="pesan" rows="6" cols="30"></textarea>
                                    </div>
                                    <div class="col-12 form-group d-none">
                                        <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                                    </div>
                                    <div class="col-12 form-group">
                                        <button class="button button-3d m-0" onclick="handle_save('#template-contactform-submit','#template-contactform','{{route('web.contact.store')}}','POST');" type="button" id="template-contactform-submit">Send Message</button>
                                    </div>
                                </div>
                                <input type="hidden" name="prefix" value="template-contactform-">
                            </form>
                        </div>
                    </div>
                    <div class="sidebar col-lg-3">
                        <address>
                            <strong>Kantor Direksi:</strong><br>
                            Jalan Letjend Jamin Ginting KM 13, no. 45 Medan<br>
                            Kelurahan Lau Cih, Kecamatan Medan Tuntungan<br>
                            Kota Medan.
                        </address>
                        <abbr title="Phone Number"><strong>Telp:</strong></abbr> (61) 8364468<br>
                        <abbr title="Email Address"><strong>Email:</strong></abbr> hello@ptpsumut.com
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>