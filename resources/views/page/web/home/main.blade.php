<x-web-layout title="Selamat datang">
    <section id="content">
        <div class="content-wrap pt-0 mt-5">
            <div class="container">
                @if ($collection->count() > 0)
                <div class="fancy-title title-border">
                    <h3>Berita Utama</h3>
                </div>
                <div class="row posts-md col-mb-30">
                    @foreach ($collection as $item)
                    @php
                    $isi = strip_tags($item->isi);
                    @endphp
                    <div class="col-lg-3 col-md-6">
                        <div class="entry">
                            <div class="entry-image">
                                <a href="{{route('web.berita.show',$item->slug)}}">
                                    <img src="{{$item->image}}" alt="{{$item->judul}}">
                                </a>
                            </div>
                            <div class="entry-title title-xs nott">
                                <h3><a href="{{route('web.berita.show',$item->slug)}}">{{$item->judul}}</a></h3>
                            </div>
                            <div class="entry-meta">
                                <ul>
                                    <li><i class="icon-calendar3"></i> {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}</li>
                                </ul>
                            </div>
                            <div class="entry-content">
                                {{Str::limit($isi, 200, '...')}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </section>
</x-web-layout>