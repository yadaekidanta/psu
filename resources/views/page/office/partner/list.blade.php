<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="text-end min-w-70px">Aksi</th>
            <th class="min-w-150px">Nama</th>
            <th class="min-w-150px">Kontak</th>
            <th class="min-w-150px">Bidang Usaha</th>
            <th class="min-w-250px">Identitas Pendiri</th>
            <th class="min-w-150px">Alamat</th>
            <th class="min-w-150px">Bank</th>
            <th class="min-w-150px">Tanggal gabung</th>
            <th class="min-w-150px">Tanggal verifikasi</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $item)
            @php
            $remove1 = Str::remove('[',$item->bidang_usaha);
            $remove2 = Str::remove('"',$remove1);
            $remove3 = Str::remove(']',$remove2);
            $kblinya = explode(',',$remove3);
            @endphp
        <tr>
            <td class="text-end">
                <div class="btn-group" role="group">
                    <button id="aksi" type="button" class="btn btn-sm btn-light btn-active-light-primary" data-bs-toggle="dropdown" aria-expanded="false">
                        Aksi
                        <span class="svg-icon svg-icon-5 m-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                            </svg>
                        </span>
                    </button>
                    <div class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" aria-labelledby="aksi">
                        <div class="menu-item px-3">
                            <a href="javascript:;" onclick="handle_open_modal('{{route('office.partner.show_doc',$item->id)}}','#modalPage','#contentListResult');" class="menu-link px-3">Lihat Dokumen {{$item->nama_perusahaan}}</a>
                        </div>
                        @if (!$item->reason && !$item->verified_at)
                        <div class="menu-item px-3">
                            <a href="javascript:;" onclick="load_input('{{route('office.partner.reason',$item->id)}}');" class="menu-link px-3">Tolak menjadi rekanan</a>
                        </div>
                        @endif
                        @if(!$item->verified_at)
                            <div class="menu-item px-3">
                                <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('office.partner.verif',$item->id)}}');" class="menu-link px-3">Verifikasi Rekanan</a>
                            </div>
                        @endif
                    </div>
                </div>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->nama}} <br>
                    {{$item->nama_perusahaan}} <br> <br>
                    @if ($item->partner_type_id)
                    {{$item->type->nama}} {{$item->type->simbol}} Rp. {{$item->type->nilai ? number_format($item->type->nilai) : 0}}
                    @endif
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->email}}<br>
                    {{$item->hp}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    @if ($item->bidang_usaha)
                    @foreach ($kbli as $kblis)
                    @if (in_array($kblis->kode, $kblinya))
                    {{$kblis->kode}} - {{$kblis->nama}} <br> <br>
                    @endif
                    @endforeach
                    @endif
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    Nama Pendiri : {{$item->nama_pendiri}} <br>
                    No KTP : {{$item->no_ktp}} <br>
                    No NPWP : {{$item->no_npwp}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->alamat}} <br>
                    @if($item->subdistrict_id)
                        {{$item->subdistrict->name}}, 
                    @endif
                    @if($item->city_id)
                        {{$item->city->name}} <br>
                    @endif
                    @if($item->province_id)
                        {{$item->province->name}}
                    @endif
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    @if ($item->bank_id)
                    {{$item->bank->nama}}<br>
                    @endif
                    No Rekening : {{$item->no_rek}} <br>
                    Atas Nama : {{$item->rek_an}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->created_at->diffForHumans()}} <br>
                    {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
                </a>
            </td>
            <td>
                @if($item->verified_at)
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->verified_at->diffForHumans()}} <br>
                    {{$item->verified_at->isoFormat('dddd,')}} {{$item->verified_at->format('j F Y')}}
                </a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}