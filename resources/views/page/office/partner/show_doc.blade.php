<div class="modal-header header-bg">
    <h2 class="text-white">{{config('app.name')}}
    <small class="ms-2 fs-7 fw-normal text-white opacity-50">Dokumen {{$partner->nama_perusahaan}}</small></h2>
    <div class="btn btn-sm btn-icon btn-color-white btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y m-5">
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr>
                <th>Jenis Dokumen</th>
                <th>Nomor</th>
                <th>Tanggal Unggah</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @foreach ($dokumen as $item)
            <tr>
                <td>{{$item->jenis->nama}}</td>
                <td>{{$item->nomor}}</td>
                <td>
                    {{$item->created_at->diffForHumans()}} <br>
                    {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
                </td>
                <td>
                    <a href="{{route('office.doc.download',$item->id)}}" class="btn btn-sm btn-primary"><i class="la la-download"></i>Unggah</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>