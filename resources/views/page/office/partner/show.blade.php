<div class="modal-header header-bg">
    <h2 class="text-white">{{config('app.name')}}
    <small class="ms-2 fs-7 fw-normal text-white opacity-50">Detil {{$partner->nama_perusahaan}}</small></h2>
    <div class="btn btn-sm btn-icon btn-color-white btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y m-5">
    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
        <thead>
            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th class="min-w-150px">Nama</th>
                <th class="min-w-150px">Kontak</th>
                <th class="min-w-150px">Bidang Usaha</th>
                <th class="min-w-250px">Identitas Pendiri</th>
                <th class="min-w-150px">Alamat</th>
                <th class="min-w-150px">Bank</th>
                <th class="min-w-150px">Tanggal gabung</th>
                <th class="min-w-150px">Tanggal verifikasi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @php
            $remove1 = Str::remove('[',$partner->bidang_usaha);
            $remove2 = Str::remove('"',$remove1);
            $remove3 = Str::remove(']',$remove2);
            $kblinya = explode(',',$remove3);
            @endphp
            <tr>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        {{$partner->nama}} <br>
                        {{$partner->nama_perusahaan}} <br> <br>
                        @if ($partner->partner_type_id)
                        {{$partner->type->nama}} {{$partner->type->simbol}} Rp. {{$partner->type->nilai ? number_format($partner->type->nilai) : 0}}
                        @endif
                    </a>
                </td>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        {{$partner->email}}<br>
                        {{$partner->hp}}
                    </a>
                </td>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        @if ($partner->bidang_usaha)
                        @foreach ($kbli as $kbli)
                        @if (in_array($kbli->kode, $kblinya))
                        {{$kbli->kode}} - {{$kbli->nama}} <br> <br>
                        @endif
                        @endforeach
                        @endif
                    </a>
                </td>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        Nama Pendiri : {{$partner->nama_pendiri}} <br>
                        No KTP : {{$partner->no_ktp}} <br>
                        No NPWP : {{$partner->no_npwp}}
                    </a>
                </td>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        {{$partner->alamat}} <br>
                        @if($partner->subdistrict_id)
                            {{$partner->subdistrict->name}}, 
                        @endif
                        @if($partner->city_id)
                            {{$partner->city->name}} <br>
                        @endif
                        @if($partner->province_id)
                            {{$partner->province->name}}
                        @endif
                    </a>
                </td>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        @if ($partner->bank_id)
                        {{$partner->bank->nama}}<br>
                        @endif
                        No Rekening : {{$partner->no_rek}} <br>
                        Atas Nama : {{$partner->rek_an}}
                    </a>
                </td>
                <td>
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        {{$partner->created_at->diffForHumans()}} <br>
                        {{$partner->created_at->isoFormat('dddd,')}} {{$partner->created_at->format('j F Y')}}
                    </a>
                </td>
                <td>
                    @if($partner->verified_at)
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                        {{$partner->verified_at->diffForHumans()}} <br>
                        {{$partner->verified_at->isoFormat('dddd,')}} {{$partner->verified_at->format('j F Y')}}
                    </a>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr>
                <th>Jenis Dokumen</th>
                <th>Nomor</th>
                <th>Tanggal Unggah</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @foreach ($dokumen as $item)
            <tr>
                <td>{{$item->jenis->nama}}</td>
                <td>{{$item->nomor}}</td>
                <td>
                    {{$item->created_at->diffForHumans()}} <br>
                    {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
                </td>
                <td>
                    <a href="{{route('office.doc.download',$item->id)}}" class="btn btn-sm btn-primary"><i class="la la-download"></i>Unggah</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>