<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-150px">Nama</th>
            <th class="min-w-150px">Contact Person</th>
            <th class="min-w-150px">Subject</th>
            <th class="min-w-150px">Pesan</th>
            <th class="min-w-150px">Tanggal Kirim</th>
            <th class="text-end min-w-70px">Aksi</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $item)
            @php
            $remove1 = Str::remove('[',$item->bidang_usaha);
            $remove2 = Str::remove('"',$remove1);
            $remove3 = Str::remove(']',$remove2);
            $kblinya = explode(',',$remove3);
            @endphp
        <tr>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->nama}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->email}}<br>
                    {{$item->hp}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->subject}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->pesan}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->created_at->diffForHumans()}} <br>
                    {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
                </a>
            </td>
            <td class="text-end">
                <a href="javascript:;" onclick="load_input('{{route('office.pesan.edit',$item->id)}}');" class="menu-link px-3">Balas Pesan</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}