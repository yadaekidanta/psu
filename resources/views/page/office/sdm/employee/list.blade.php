<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-50px">NIP - Nama</th>
            <th class="min-w-50px">Kontak Person</th>
            <th class="min-w-50px">Tmpt & Tgl Lahir</th>
            <th class="min-w-50px">Tgl Pengangkatan</th>
            <th class="min-w-50px">Jenis Kelamin</th>
            <th class="min-w-50px">Agama</th>
            <th class="min-w-50px">Alamat Identitas</th>
            <th class="min-w-50px">Alamat Domisili</th>
            <th class="min-w-50px">Lokasi Unit - Divisi / Bagian - Jabatan</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $item)
        <tr>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    @if ($item->nip)
                    {{$item->nip}} -
                    @endif
                    {{$item->nama}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->email}} <br>
                    {{$item->hp}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->tempat_lahir}},  {{$item->tanggal_lahir->isoFormat('dddd,')}} {{$item->tanggal_lahir->format('j F Y')}} <br>
                    @if($item->tanggal_lahir->age > 0)
                    {{$item->tanggal_lahir->age}} Tahun
                    @endif
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->tanggal_pengangkatan->isoFormat('dddd,')}} {{$item->tanggal_pengangkatan->format('j F Y')}} <br>
                    @if($item->tanggal_pengangkatan->age > 0)
                        {{$item->tanggal_pengangkatan->age}} Tahun Lalu
                    @endif
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->jk}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->agama}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->alamat}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->alamat_domisili}}
                </a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                    {{$item->lokasi->nama}} - {{$item->divisi->nama}} - {{$item->role->nama}}
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}