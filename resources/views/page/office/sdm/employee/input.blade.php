
<div class="toolbar d-flex flex-stack mb-3 mb-lg-5" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-5 py-2">
            <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Pegawai</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('office.dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">SDM</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Pegawai</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">
                    @if ($data->id)
                    Ubah
                    @else
                    Tambah
                    @endif
                    Data
                </li>
            </ul>
        </div>
        <div class="d-flex align-items-center py-2">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-info">Kembali</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-body pb-5">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Lokasi</label>
                            <select class="form-select" id="lokasi" name="lokasi">
                                <option SELECTED DISABLED>Pilih Lokasi</option>
                                @foreach ($lokasi as $item)
                                    <option value="{{$item->id}}" {{$data->lokasi_id == $item->id ? 'selected' : ''}}>{{$item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Divisi / Bagian</label>
                            <select class="form-select" id="divisi" name="divisi"></select>
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Jabatan</label>
                            <select class="form-select" id="jabatan" name="jabatan"></select>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-lg-2">
                            <label class="fs-6 fw-bold mb-2">NIP</label>
                            <input type="text" class="form-control" name="nip" placeholder="Masukkan NIP..." value="{{$data->nip}}">
                        </div>
                        <div class="col-lg-3">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama..." value="{{$data->nama}}">
                        </div>
                        <div class="col-lg-3">
                            <label class="required fs-6 fw-bold mb-2">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Masukkan email..." value="{{$data->email}}">
                        </div>
                        <div class="col-lg-2">
                            <label class="required fs-6 fw-bold mb-2">No HP</label>
                            <input type="tel" maxlength="13" class="form-control" name="hp" placeholder="Masukkan No HP..." value="{{$data->hp}}">
                        </div>
                        <div class="col-lg-2">
                            <label class="required fs-6 fw-bold mb-2">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Masukkan Password...">
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-lg-2">
                            <label class="fs-6 fw-bold mb-2">Tmpt Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" placeholder="Masukkan tempat lahir..." value="{{$data->tempat_lahir}}">
                        </div>
                        <div class="col-lg-2">
                            <label class="fs-6 fw-bold mb-2">Tgl Lahir</label>
                            <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukkan tanggal lahir..." value="{{$data->tanggal_lahir}}">
                        </div>
                        <div class="col-lg-3">
                            <label class="fs-6 fw-bold mb-2">Tgl Pengangkatan</label>
                            <input type="text" class="form-control" id="tanggal_pengangkatan" name="tanggal_pengangkatan" placeholder="Masukkan tanggal pengangkatan..." value="{{$data->tanggal_pengangkatan}}">
                        </div>
                        <div class="col-lg-3">
                            <label class="fs-6 fw-bold mb-2">Jenis Kelamin</label>
                            <select class="form-control" name="jenis_kelamin" id="jk">
                                <option value="">Pilih Jenis Kelamin</option>
                                <option value="Laki-Laki" {{$data->jk == "Laki-Laki" ? 'selected' : ''}}>Laki-Laki</option>
                                <option value="Perempuan" {{$data->jk == "Perempuan" ? 'selected' : ''}}>Perempuan</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <label class="fs-6 fw-bold mb-2">Agama</label>
                            <select class="form-control" name="agama" id="agama">
                                <option value="">Pilih Agama</option>
                                <option value="Islam" {{$data->agama == "Islam" ? 'selected' : ''}}>Islam</option>
                                <option value="Katolik" {{$data->agama == "Katolik" ? 'selected' : ''}}>Katolik</option>
                                <option value="Protestan" {{$data->agama == "Protestan" ? 'selected' : ''}}>Protestan</option>
                                <option value="Hindu" {{$data->agama == "Hindu" ? 'selected' : ''}}>Hindu</option>
                                <option value="Buddha" {{$data->agama == "Buddha" ? 'selected' : ''}}>Buddha</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-lg-6">
                            <label class="fs-6 fw-bold mb-2">Alamat Identitas</label>
                            <textarea class="form-control" name="alamat" id="alamat">{{$data->alamat}}</textarea>
                        </div>
                        <div class="col-lg-6">
                            <label class="fs-6 fw-bold mb-2">Alamat Domisili</label>
                            <textarea class="form-control" name="alamat_domisili" id="alamat_domisili">{{$data->alamat_domisili}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.employee.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.employee.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_select('lokasi','Pilih Lokasi');
    obj_select('divisi','Pilih Divisi');
    obj_select('jabatan','Pilih Jabatan');
    obj_select('jk','Pilih Jenis Kelamin');
    obj_select('agama','Pilih Agama');
    obj_enddatenow('tanggal_lahir');
    obj_enddatenow('tanggal_pengangkatan');
    $("#lokasi").change(function(){
        $.post('{{route('office.divisi.list_option')}}', {lokasi:$("#lokasi").val()}, function(result) {
            $("#divisi").html(result);
        }, "html");
    });
    $("#divisi").change(function(){
        $.post('{{route('office.role.list_option')}}', {divisi:$("#divisi").val()}, function(result) {
            $("#jabatan").html(result);
        }, "html");
    });
    @if($data->id)
    setTimeout(function(){ 
        $('#lokasi').trigger('change');
        setTimeout(function(){ 
            $('#divisi').val('{{$data->divisi_id}}');
            $('#divisi').trigger('change');
            setTimeout(function(){ 
                $('#jabatan').val('{{$data->role_id}}');
                $('#jabatan').trigger('change');
            }, 2000);
        }, 1500);
    }, 1000);
    @endif
</script>