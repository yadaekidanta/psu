<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-70px">Aksi</th>
            <th class="min-w-125px">Jenis</th>
            <th class="max-w-125px">Isi</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $item)
        @php
        $isi = strip_tags($item->isi);
        @endphp
        <tr>
            <td class="">
                <a href="javascript:;" onclick="load_input('{{route('office.konten.eproc.edit',$item->id)}}');" class="menu-link px-3">Ubah</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->tipe}}</a>
            </td>
            <td>
                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{Str::limit($isi,200)}}</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.office.pagination')}}