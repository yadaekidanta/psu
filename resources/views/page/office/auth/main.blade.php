<x-auth-layout title="Halaman Masuk">
    <div id="page_login">
        <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
            <form class="form w-100" novalidate="novalidate" id="form_login">
                <div class="text-center mb-10">
                    <h1 class="text-dark mb-3">{{config('app.name')}}</h1>
                    <div class="text-gray-400 fw-bold fs-4 d-none">New Here?
                    <a href="../../demo10/dist/authentication/flows/basic/sign-up.html" class="link-primary fw-bolder">Create an Account</a></div>
                </div>
                <div class="fv-row mb-10">
                    <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                    <input class="form-control form-control-lg form-control-solid" type="email" id="email" name="email" autocomplete="off" data-login="1" />
                </div>
                <div class="fv-row mb-10">
                    <div class="d-flex flex-stack mb-2">
                        <label class="form-label fw-bolder text-dark fs-6 mb-0">Kata Sandi</label>
                        <a href="javascript:;" onclick="auth_content('page_forgot');" class="link-success fs-6 fw-bolder">Lupa kata sandi ?</a>
                    </div>
                    <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" data-login="2" />
                </div>
                <div class="text-center">
                    <button type="button" onclick="handle_post('#tombol_login','#form_login','{{route('office.auth.login')}}');" id="tombol_login" class="btn btn-lg btn-success w-100 mb-5" data-login="3">
                        <span class="indicator-label">Lanjutkan</span>
                        <span class="indicator-progress">Harap tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="page_forgot">
        <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
            <form class="form w-100" novalidate="novalidate" id="form_forgot">
                <div class="text-center mb-10">
                    <h1 class="text-dark mb-3">{{config('app.name')}}</h1>
                </div>
                <div class="fv-row mb-10">
                    <div class="d-flex flex-stack mb-2">
                        <label class="form-label fw-bolder text-dark fs-6 mb-0">Email</label>
                        <a href="javascript:;" onclick="auth_content('page_login');" class="link-success fs-6 fw-bolder">Ingat kata sandi ?</a>
                    </div>
                    <input class="form-control form-control-lg form-control-solid" type="email" id="mail" name="email" autocomplete="off" data-forgot="1" />
                </div>
                <div class="text-center">
                    <button type="button" onclick="handle_post('#tombol_forgot','#form_forgot','{{route('office.auth.forgot')}}');" id="tombol_forgot" class="btn btn-lg btn-success w-100 mb-5" data-forgot="2">
                        <span class="indicator-label">Lanjutkan</span>
                        <span class="indicator-progress">Harap tunggu...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    @section('custom_js')
    <script>
        auth_content('page_login');
    </script> 
    @endsection
</x-auth-layout>