<div class="modal-header header-bg">
    <h2 class="text-white">{{config('app.name')}}
    <small class="ms-2 fs-7 fw-normal text-white opacity-50">Daftar Penawaran {{$data->judul}}</small></h2>
    <div class="btn btn-sm btn-icon btn-color-white btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y m-5">
    <h1>Nilai Tender : {{number_format($data->nilai_tender)}}</h1>
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr>
                <th>Nama Rekanan</th>
                <th>Nilai penawaran</th>
                <th>Status</th>
                <th>Tanggal kirim penawaran</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @foreach ($data->list_penawaran as $item)
            @php
            $remove1 = Str::remove('[',$item->partner->bidang_usaha);
            $remove2 = Str::remove('"',$remove1);
            $remove3 = Str::remove(']',$remove2);
            $kblinya = explode(',',$remove3);
            @endphp
            <tr>
                <td>
                    <a href="javascript:;" onclick="handle_open_modal('{{route('office.partner.show_detil',$item->partner_id)}}','#modalPage','#contentListResult');">
                        {{$item->partner->nama_perusahaan}}
                    </a>
                </td>
                <td>{{number_format($item->nilai)}}</td>
                <td>
                    {{$item->st}}
                </td>
                <td>
                    {{$item->created_at->diffForHumans()}} <br>
                    {{$item->created_at->isoFormat('dddd,')}} {{$item->created_at->format('j F Y')}}
                </td>
                <td>
                    @if ($item->file)
                    <a href="{{route('office.offer.download',$item->id)}}" class="btn btn-sm btn-primary"><i class="la la-download"></i>Unggah</a>
                    @endif
                    @if ($data->st == "Terpublikasi")
                        @if ($item->st == "Penawaran terkirim")
                        <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('office.offer.revisi',$item->id)}}');" class="btn btn-sm btn-primary"><i class="la la-forward"></i>Revisi</a>
                        <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('office.offer.acc',$item->id)}}');" class="btn btn-sm btn-success"><i class="la la-check"></i>Pemenang</a>
                    </div>
                    @endif
                @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>