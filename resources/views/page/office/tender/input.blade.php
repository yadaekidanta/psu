
<div class="toolbar d-flex flex-stack mb-3 mb-lg-5" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-5 py-2">
            <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Tender</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('office.dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Tender</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">
                    @if ($data->id)
                        @if ($data->st == "Draft")
                        Ubah
                        @else
                        Unggah File SPK
                        @endif
                    @else
                    Tambah
                    @endif
                    @if ($data->st == "Draft")
                    Data
                    @endif
                </li>
            </ul>
        </div>
        <div class="d-flex align-items-center py-2">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-info">Kembali</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-body pb-5">
                <form id="form_input">
                    <div class="row">
                        @if(!$data->id || $data->st == "Draft")
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Judul</label>
                            <input type="text" class="form-control" name="judul" placeholder="Masukkan judul..." value="{{$data->judul}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="{{$data->id ? '' : 'required'}} fs-6 fw-bold mb-2">File KAK</label>
                            <input type="file" class="form-control" name="attachment">
                        </div>
                        <div class="col-lg-4 mt-5">
                            <label class="required fs-6 fw-bold mb-2">Nilai Tender</label>
                            <input type="text" class="form-control" id="nilai_tender" name="nilai_tender" value="{{$data->nilai_tender ? number_format($data->nilai_tender): 0}}">
                        </div>
                        <div class="col-lg-4 mt-5">
                            <label class="required fs-6 fw-bold mb-2">Tanggal Akhir Pendaftaran Tender</label>
                            <input type="text" class="form-control" id="tender_deadline" name="tender_deadline" value="{{$data->tender_deadline_at}}">
                        </div>
                        <div class="col-lg-4 mt-5">
                            <label class="required fs-6 fw-bold mb-2">Tanggal Akhir Pengerjaan Tender</label>
                            <input type="text" class="form-control" id="work_deadline" name="work_deadline" value="{{$data->work_deadline_at}}">
                        </div>
                        <div class="col-lg-12 mt-5 mb-10">
                            <label class="required fs-6 fw-bold mb-2">Deskripsi</label>
                            <div id="deskripsi">{!!$data->deskripsi!!}</div>
                            <textarea style="display:none" name="content">{!!$data->deskripsi!!}</textarea>
                        </div>
                        @endif
                        @if($data->st == "Berjalan")
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">No SPK</label>
                            <input type="text" class="form-control" name="no_spk" placeholder="Masukkan No SPK..." value="{{$data->no_spk}}">
                        </div>
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">File SPK</label>
                            <input type="file" class="form-control" name="file_spk">
                        </div>
                        @endif
                        <div class="min-w-150px mt-15 text-end">
                            @if ($data->id)
                                @if ($data->st == "Draft")
                                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.tender.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                                @else
                                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.tender.update_spk',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Kirim</button>
                                @endif
                            @else
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.tender.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_quill('deskripsi');
    number_only('nilai_tender');
    ribuan('nilai_tender');
    obj_startdatenow('tender_deadline');
    obj_startdatenow('work_deadline');
</script>