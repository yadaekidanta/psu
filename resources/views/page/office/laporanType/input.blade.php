
<div class="toolbar d-flex flex-stack mb-3 mb-lg-5" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-5 py-2">
            <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Jenis Laporan</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('office.dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Jenis Laporan</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">
                    @if ($data->id)
                    Ubah
                    @else
                    Tambah
                    @endif
                    Data
                </li>
            </ul>
        </div>
        <div class="d-flex align-items-center py-2">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-info">Kembali</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-body pb-5">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama..." value="{{$data->nama}}">
                        </div>
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.laporan-type.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('office.laporan-type.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>