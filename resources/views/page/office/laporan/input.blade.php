
<div class="toolbar d-flex flex-stack mb-3 mb-lg-5" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack flex-wrap">
        <div class="page-title d-flex flex-column me-5 py-2">
            <h1 class="d-flex flex-column text-dark fw-bolder fs-3 mb-0">Laporan</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 pt-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('office.dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Laporan</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">
                    @if ($data->id)
                    Ubah
                    @else
                    Tambah
                    @endif
                    Data
                </li>
            </ul>
        </div>
        <div class="d-flex align-items-center py-2">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-info">Kembali</a>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-body pb-5">
                <form id="form_input">
                    <div class="row">
                        <div class="col-lg-3">
                            <label class="required fs-6 fw-bold mb-2">Jenis Laporan</label>
                            <select class="form-select" id="jenis_laporan" name="jenis_laporan">
                                <option value="">Pilih Jenis Laporan</option>
                                @foreach ($type as $item)
                                <option value="{{$item->id}}" {{$data->laporan_type_id == $item->id ? 'selected' : ''}}>{{$item->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class="required fs-6 fw-bold mb-2">Judul</label>
                            <input type="text" class="form-control" name="judul" placeholder="Masukkan judul..." value="{{$data->judul}}">
                        </div>
                        <div class="col-lg-3">
                            <label class="required fs-6 fw-bold mb-2">Tahun</label>
                            <input type="text" maxlength="4" class="form-control" id="tahun" name="tahun" value="{{$data->tahun}}">
                        </div>
                        <div class="col-lg-3">
                            <label class="{{$data->id ? '' : 'required'}} fs-6 fw-bold mb-2">File</label>
                            <input type="file" accept="application/pdf" class="form-control" name="file">
                        </div>
                        <div class="col-lg-8 mt-3 mb-15">
                            <label class="required fs-6 fw-bold mb-2">Isi</label>
                            <div id="isi">{!!$data->isi!!}</div>
                            <textarea style="display:none" name="content">{!!$data->isi!!}</textarea>
                        </div>
                        <div class="min-w-150px mt-15 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.laporan.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('office.laporan.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    obj_select("jenis_laporan","Pilih Jenis Laporan");
    number_only("tahun");
    obj_year("tahun");
    obj_quill('isi');
</script>